<?php

namespace Drupal\Tests\entity_contact\Functional;

use Drupal\Core\Url;
use Drupal\entity_contact\EntityContactMessageInterface;

/**
 * Test the entity_contact_route module.
 *
 * @group entity_contact_test
 */
class EntityContactTest extends EntityContactTestBase {

  /**
   * Test the contact form route permissions.
   */
  public function testRoutePermissions(): void {
    // Anonymous user cannot administer.
    $paths = [
      'admin/content/entity-contact' => 403,
      'admin/content/entity-contact/add' => 403,
      'admin/content/entity-contact/manage/feedback' => 403,
      'admin/content/entity-contact/manage/feedback/delete' => 403,
      'admin/content/entity-contact/manage/feedback/submissions' => 403,
      'admin/content/entity-contact/submissions/1' => 403,
      'admin/content/entity-contact/submissions/1/edit' => 403,
      'admin/content/entity-contact/submissions/1/delete' => 403,
    ];

    foreach ($paths as $path => $status_code) {
      $this->drupalGet($path);
      $this->assertEquals($status_code, $this->getSession()
        ->getStatusCode(), $path . ' is not ' . $status_code);
    }

    user_role_grant_permissions('anonymous', ['access entity contact form']);
    foreach ($paths as $path => $status_code) {
      $this->drupalGet($path);
      $this->assertEquals($status_code, $this->getSession()
        ->getStatusCode(), $path . ' is not ' . $status_code);
    }

    // Create and log in as user which can view submissions.
    $admin_user = $this->drupalCreateUser([
      'view entity contact form submissions',
      'access entity contact form',
    ]);
    $this->drupalLogin($admin_user);

    $paths = [
      'admin/content/entity-contact' => 200,
      'admin/content/entity-contact/add' => 403,
      'admin/content/entity-contact/manage/feedback' => 403,
      'admin/content/entity-contact/manage/feedback/delete' => 403,
      'admin/content/entity-contact/manage/feedback/submissions' => 200,
      'admin/content/entity-contact/submissions/1' => 200,
      'admin/content/entity-contact/submissions/1/edit' => 403,
      'admin/content/entity-contact/submissions/1/delete' => 403,
    ];

    foreach ($paths as $path => $status_code) {
      $this->drupalGet($path);
      $this->assertEquals($status_code, $this->getSession()
        ->getStatusCode(), $path . ' is not ' . $status_code);
    }

    // Create and log in as user which can administer submissions.
    $admin_user = $this->drupalCreateUser([
      'administer entity contact form submissions',
      'access entity contact form',
    ]);
    $this->drupalLogin($admin_user);

    $paths = [
      'admin/content/entity-contact' => 403,
      'admin/content/entity-contact/add' => 403,
      'admin/content/entity-contact/manage/feedback' => 403,
      'admin/content/entity-contact/manage/feedback/delete' => 403,
      'admin/content/entity-contact/manage/feedback/submissions' => 200,
      'admin/content/entity-contact/submissions/1' => 200,
      'admin/content/entity-contact/submissions/1/edit' => 200,
      'admin/content/entity-contact/submissions/1/delete' => 200,
    ];

    foreach ($paths as $path => $status_code) {
      $this->drupalGet($path);
      $this->assertEquals($status_code, $this->getSession()
        ->getStatusCode(), $path . ' is not ' . $status_code);
    }

    // Create and log in administrative user.
    $admin_user = $this->drupalCreateUser([
      'administer entity contact forms',
      'access entity contact form',
    ]);
    $this->drupalLogin($admin_user);

    $paths = [
      'admin/content/entity-contact' => 200,
      'admin/content/entity-contact/add' => 200,
      'admin/content/entity-contact/manage/feedback' => 200,
      'admin/content/entity-contact/manage/feedback/delete' => 200,
      'admin/content/entity-contact/manage/feedback/submissions' => 403,
      'admin/content/entity-contact/submissions/1' => 403,
      'admin/content/entity-contact/submissions/1/edit' => 403,
      'admin/content/entity-contact/submissions/1/delete' => 403,
    ];

    foreach ($paths as $path => $status_code) {
      $this->drupalGet($path);
      $this->assertEquals($status_code, $this->getSession()
        ->getStatusCode(), $path . ' is not ' . $status_code);
    }
  }

  /**
   * Test the form functionalities.
   */
  public function testForm(): void {
    $this->drupalLogin($this->adminUser);

    // Create a form.
    $id = strtolower($this->randomMachineName(8));
    $label = 'Label ' . $this->randomMachineName();
    $description = 'Description ' . $this->randomMachineName();
    $message = 'Message ' . $this->randomMachineName();
    $redirect = '/user/' . $this->adminUser->id();
    $submit_button_text = 'Submit button text' . $this->randomMachineName();

    $this->drupalGet('contact-form/' . $id);
    $this->assertSession()->statusCodeEquals(404);

    $this->drupalGet('admin/content/entity-contact/add');

    $this->submitForm([
      'id' => $id,
      'label' => $label,
      'description[value]' => $description,
      'message' => $message,
      'redirect' => $redirect,
      'submit_button_text' => $submit_button_text,
      'store_submissions' => FALSE,
    ], 'Save');

    // Create a simple textfield.
    $field_name = mb_strtolower($this->randomMachineName());
    $field_label = $this->randomMachineName();
    $this->fieldUIAddNewField('admin/content/entity-contact/manage/' . $id, $field_name, $field_label, 'string');

    drupal_flush_all_caches();

    $field_name = 'field_' . $field_name;
    $field_name_value = $this->randomMachineName();

    $this->drupalGet('contact-form/' . $id);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($label);
    $this->assertSession()->pageTextContains($description);
    $this->assertSession()->buttonExists($submit_button_text);

    // Submit a form.
    $this->submitForm([
      $field_name . '[0][value]' => $field_name_value,
    ], $submit_button_text);
    $this->assertSession()->pageTextContains($message);
    $this->assertSession()->addressEquals(ltrim($redirect, '/'));
    $this->assertCount(0, $this->messageStorage->loadByProperties(['entity_contact_form' => $id]));

    // Test storage.
    $this->drupalGet('admin/content/entity-contact/manage/' . $id);
    $this->submitForm([
      'store_submissions' => TRUE,
    ], 'Save');

    $url = Url::fromRoute('entity_contact_route', ['entity_contact_form' => $id], [
      'query' => [
        'gclid' => 'gclid1',
        'utm_campaign' => 'campaign',
        'utm_content' => 'content',
        'utm_medium' => 'medium',
        'utm_source' => 'source',
        'utm_term' => 'term',
      ],
    ]);

    $this->drupalGet($url);

    $this->submitForm([
      $field_name . '[0][value]' => $field_name_value,
    ], $submit_button_text);

    /**
     * @var \Drupal\entity_contact\EntityContactMessageInterface[] $messages
     */
    $messages = array_values($this->messageStorage->loadByProperties(['entity_contact_form' => $id]));
    $this->assertCount(1, $messages);
    $this->assertInstanceOf(EntityContactMessageInterface::class, $messages[0]);
    $this->assertEquals($field_name_value, $messages[0]->get($field_name)
      ->getString());
    $this->assertTrue($messages[0]->get('ip_address')->isEmpty());
    $this->assertEquals('source', $messages[0]->get('utm_source')->getString());
    $this->assertEquals('medium', $messages[0]->get('utm_medium')->getString());
    $this->assertEquals('campaign', $messages[0]->get('utm_campaign')
      ->getString());
    $this->assertEquals('content', $messages[0]->get('utm_content')
      ->getString());
    $this->assertEquals('term', $messages[0]->get('utm_term')->getString());
    $this->assertEquals('gclid1', $messages[0]->get('gclid')->getString());
    $this->assertEquals($url->setAbsolute()
      ->toString(TRUE)
      ->getGeneratedUrl(), $messages[0]->get('submission_url')->getString());

    // Deleting the form should also delete any message entities.
    $messages[0]->getContactForm()->delete();
    $this->assertCount(0, $this->messageStorage->loadByProperties(['entity_contact_form' => $id]));
  }

  /**
   * Tests the hasFields method.
   */
  public function testHasFields(): void {
    $this->drupalLogin($this->adminUser);

    // Create a form.
    $id = strtolower($this->randomMachineName(8));

    $this->drupalGet('admin/content/entity-contact/add');
    $this->submitForm([
      'id' => $id,
      'label' => $id,
    ], 'Save');

    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
     */
    $entity_contact_form = $this->formStorage->load($id);
    $this->assertFalse($entity_contact_form->hasFields());

    // Create a simple textfield.
    $field_name = mb_strtolower($this->randomMachineName());
    $field_label = $this->randomMachineName();
    $this->fieldUIAddNewField('admin/content/entity-contact/manage/' . $id, $field_name, $field_label, 'string');

    drupal_flush_all_caches();

    $this->assertTrue($entity_contact_form->hasFields());
  }

  /**
   * Test storage of the submitter's IP address.
   */
  public function testStoreIpAddressSetting(): void {
    $this->drupalLogin($this->adminUser);

    // Create a form.
    $id = strtolower($this->randomMachineName(8));

    $this->drupalGet('admin/content/entity-contact/add');
    $this->submitForm([
      'id' => $id,
      'label' => $id,
      'store_submissions' => TRUE,
    ], 'Save');

    // IP storage is disabled by module-default.
    $this->drupalGet(Url::fromRoute('entity_contact_route', ['entity_contact_form' => $id]));
    $this->submitForm([], 'Send');
    /**
     * @var \Drupal\entity_contact\EntityContactMessageInterface[] $messages
     */
    $messages = array_values($this->messageStorage->loadByProperties(['entity_contact_form' => $id]));
    $this->assertCount(1, $messages);
    $this->assertTrue($messages[0]->get('ip_address')->isEmpty());
    $this->messageStorage->delete($messages);

    // Enable IP storage.
    $this->drupalGet('admin/content/entity-contact/settings');
    $this->submitForm([
      'entity_contact_message_store_ip_address' => TRUE,
    ], 'Save configuration');

    $this->drupalGet(Url::fromRoute('entity_contact_route', ['entity_contact_form' => $id]));
    $this->submitForm([], 'Send');
    /**
     * @var \Drupal\entity_contact\EntityContactMessageInterface[] $messages
     */
    $messages = array_values($this->messageStorage->loadByProperties(['entity_contact_form' => $id]));
    $this->assertCount(1, $messages);
    $this->assertFalse($messages[0]->get('ip_address')->isEmpty());
    $this->messageStorage->delete($messages);

    // Disable IP storage again.
    $this->drupalGet('admin/content/entity-contact/settings');
    $this->submitForm([
      'entity_contact_message_store_ip_address' => FALSE,
    ], 'Save configuration');

    $this->drupalGet(Url::fromRoute('entity_contact_route', ['entity_contact_form' => $id]));
    $this->submitForm([], 'Send');
    /**
     * @var \Drupal\entity_contact\EntityContactMessageInterface[] $messages
     */
    $messages = array_values($this->messageStorage->loadByProperties(['entity_contact_form' => $id]));
    $this->assertCount(1, $messages);
    $this->assertTrue($messages[0]->get('ip_address')->isEmpty());
    $this->messageStorage->delete($messages);
  }

  /**
   * Tests the delete all submissions form.
   */
  public function testDeleteAllSubmissions(): void {
    // Create and log in as user which can administer submissions.
    $admin_user = $this->drupalCreateUser([
      'administer entity contact form submissions',
      'administer entity contact forms',
      'access entity contact form',
    ]);
    $this->drupalLogin($admin_user);

    $form1 = strtolower($this->randomMachineName(8));

    $this->drupalGet('admin/content/entity-contact/add');
    $this->submitForm([
      'id' => $form1,
      'label' => $form1,
      'store_submissions' => TRUE,
    ], 'Save');

    $form2 = strtolower($this->randomMachineName(8));

    $this->drupalGet('admin/content/entity-contact/add');
    $this->submitForm([
      'id' => $form2,
      'label' => $form2,
      'store_submissions' => TRUE,
    ], 'Save');

    $this->drupalGet(Url::fromRoute('entity_contact_route', ['entity_contact_form' => $form1]));
    $this->submitForm([], 'Send');

    $this->drupalGet(Url::fromRoute('entity_contact_route', ['entity_contact_form' => $form2]));
    $this->submitForm([], 'Send');

    $this->assertCount(1, $this->messageStorage->loadByProperties(['entity_contact_form' => $form1]));
    $this->assertCount(1, $this->messageStorage->loadByProperties(['entity_contact_form' => $form2]));

    $this->drupalGet(Url::fromRoute('entity_contact.delete_submissions', ['entity_contact_form' => $form1]));
    $this->submitForm([], 'Confirm');

    $this->assertCount(0, $this->messageStorage->loadByProperties(['entity_contact_form' => $form1]));
    $this->assertCount(1, $this->messageStorage->loadByProperties(['entity_contact_form' => $form2]));
  }

}

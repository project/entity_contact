<?php

namespace Drupal\Tests\entity_contact\Functional;

use Drupal\entity_contact\Entity\EntityContactMessage;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;

/**
 * Base test class for entity_contact functional tests.
 *
 * @group entity_contact_test
 */
abstract class EntityContactTestBase extends BrowserTestBase {

  use FieldUiTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'text',
    'entity_contact',
    'entity_contact_test',
    'entity_contact_route',
    'field_ui',
    'block',
    'filter',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Form storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $formStorage;

  /**
   * Message storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $messageStorage;

  /**
   * Test filter format.
   *
   * @var \Drupal\filter\Entity\FilterFormat
   */
  protected $filter;

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    EntityContactMessage::create([
      'entity_contact_form' => 'feedback',
    ])->save();

    $this->formStorage = $this->container->get('entity_type.manager')
      ->getStorage('entity_contact_form');
    $this->messageStorage = $this->container->get('entity_type.manager')
      ->getStorage('entity_contact_message');

    $this->filter = FilterFormat::create([
      'format' => 'html',
      'name' => 'html',
      'status' => TRUE,
    ]);
    $this->filter->save();

    $this->adminUser = $this->drupalCreateUser([
      'administer entity contact forms',
      'administer entity contact form settings',
      'access entity contact form',
      $this->filter->getPermissionName(),
      'administer entity_contact_message display',
      'administer entity_contact_message fields',
      'administer entity_contact_message form display',
    ]);
  }

}

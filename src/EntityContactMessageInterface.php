<?php

namespace Drupal\entity_contact;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a contact message entity.
 */
interface EntityContactMessageInterface extends ContentEntityInterface {

  /**
   * Returns the form this contact message belongs to.
   *
   * @return \Drupal\entity_contact\EntityContactFormInterface
   *   The contact form entity.
   */
  public function getContactForm();

  /**
   * Returns the time that the message was created.
   *
   * @return int
   *   The timestamp of when the message was created.
   */
  public function getCreatedTime();

}

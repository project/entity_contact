<?php

namespace Drupal\entity_contact\Routing;

use Drupal\entity_contact\SubmissionHandlerPluginManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\entity_contact\Controller\SubmissionHandlerController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Router that generates routes for plugin settings.
 */
class Router implements ContainerInjectionInterface {

  /**
   * The remote storage plugin manager.
   *
   * @var \Drupal\entity_contact\SubmissionHandlerPluginManager
   */
  protected $submissionHandlerPluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\entity_contact\SubmissionHandlerPluginManager $submissionHandlerPluginManager
   *   The plugin manager.
   */
  public function __construct(SubmissionHandlerPluginManager $submissionHandlerPluginManager) {
    $this->submissionHandlerPluginManager = $submissionHandlerPluginManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entity_contact.submission_handler')
    );
  }

  /**
   * Generate routes for all submission handlers.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   The routes.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function routes(): RouteCollection {
    $routes = new RouteCollection();

    foreach ($this->submissionHandlerPluginManager->getDefinitions() as $plugin_id => $definition) {
      $routes->add(
        'entity_contact.submission_handler.' . $plugin_id,
        new Route(
          '/admin/content/entity-contact/manage/{entity_contact_form}/handlers/' . $plugin_id,
          [
            '_controller' => SubmissionHandlerController::class . '::form',
            '_title_callback' => SubmissionHandlerController::class . '::title',
          ],
          [
            '_permission' => 'administer entity contact forms',
          ],
          [
            'parameters' => [
              'entity_contact_form' => [
                'type' => 'entity:entity_contact_form',
              ],
            ],
          ]
        )
      );
    }

    return $routes;
  }

}

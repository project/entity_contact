<?php

namespace Drupal\entity_contact;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for contact message forms.
 *
 * @internal
 */
class EntityContactMessageForm extends ContentEntityForm {

  /**
   * The message being used by this form.
   *
   * @var \Drupal\entity_contact\EntityContactMessageInterface
   */
  protected $entity;

  /**
   * The flood control mechanism.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a MessageForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood control mechanism.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, FloodInterface $flood, LanguageManagerInterface $language_manager, DateFormatterInterface $date_formatter, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->flood = $flood;
    $this->languageManager = $language_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('flood'),
      $container->get('language_manager'),
      $container->get('date.formatter'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $message = $this->entity;
    $form = parent::form($form, $form_state);
    $form['#attributes']['class'][] = 'contact-form';

    if (!empty($message->preview)) {
      $form['preview'] = [
        '#theme_wrappers' => ['container__preview'],
        '#attributes' => ['class' => ['preview']],
      ];
      $form['preview']['message'] = $this->entityTypeManager->getViewBuilder('entity_contact_message')
        ->view($message, 'full');
    }

    $form['#entity_contact_mesasge'] = $message;
    $form['#entity_contact_form'] = $message->getContactForm();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $elements = parent::actions($form, $form_state);
    $elements['submit']['#value'] = $this->t('Send message');
    $elements['preview'] = [
      '#type' => 'submit',
      '#value' => $this->t('Preview'),
      '#submit' => ['::submitForm', '::preview'],
      '#access' => !empty($form_state->getStorage()['form_display']->getComponent('preview')),
    ];

    $submit_button_text = $this->entity->getContactForm()
      ->getSubmitButtonText();
    if (!empty($submit_button_text)) {
      $elements['submit']['#value'] = $submit_button_text;
    }

    return $elements;
  }

  /**
   * Form submission handler for the 'preview' action.
   */
  public function preview(array $form, FormStateInterface $form_state) {
    $message = $this->entity;
    $message->preview = TRUE;
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $message = parent::validateForm($form, $form_state);

    // Check if flood control has been activated.
    if (!$this->currentUser()
      ->hasPermission('administer entity contact forms')) {
      $limit = $this->config('entity_contact.settings')->get('flood.limit');
      $interval = $this->config('entity_contact.settings')
        ->get('flood.interval');

      if (!$this->flood->isAllowed('entity_contact', $limit, $interval)) {
        $form_state->setErrorByName('', $this->t('You cannot send more than %limit messages in @interval. Try again later.', [
          '%limit' => $limit,
          '@interval' => $this->dateFormatter->formatInterval($interval),
        ]));
      }
    }

    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $message = $this->entity;
    $result = $message->save();
    $contact_form = $message->getContactForm();

    $this->flood->register('entity_contact', $this->config('entity_contact.settings')
      ->get('flood.interval'));
    if ($submission_message = $contact_form->getMessage()) {
      $this->messenger()->addStatus($submission_message);
    }

    $form_state->setRedirectUrl($contact_form->getRedirectUrl());

    return $result;
  }

}

<?php

namespace Drupal\entity_contact\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Delete all submissions confirmation form.
 */
class DeleteAllSubmissionsConfirmForm extends ConfirmFormBase {

  /**
   * Submission storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityContactMessageStorage;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityContactMessageStorage = $entityTypeManager->getStorage('entity_contact_message');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getQuestion() {
    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
     */
    $entity_contact_form = $this->getRequest()->get('entity_contact_form');
    return $this->t('Are you sure you want to delete all submissions of the <em>@form</em> form?', ['@form' => $entity_contact_form->label()]);
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl() {
    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
     */
    $entity_contact_form = $this->getRequest()->get('entity_contact_form');
    return Url::fromRoute('entity_contact_form.submissions', ['entity_contact_form' => $entity_contact_form->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Confirm');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritDoc}
   */
  public function getFormName() {
    return 'confirm';
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'entity_contact_delete_all_submissions';
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
     */
    $entity_contact_form = $this->getRequest()->get('entity_contact_form');

    $submissions = $this->entityContactMessageStorage->loadByProperties([
      'entity_contact_form' => $entity_contact_form->id(),
    ]);
    $this->entityContactMessageStorage->delete($submissions);

    $this->messenger()
      ->addStatus($this->t('@count submissions have been deleted.', ['@count' => count($submissions)]));
    $form_state->setRedirectUrl(Url::fromRoute('entity_contact_form.submissions', ['entity_contact_form' => $entity_contact_form->id()]));
  }

}

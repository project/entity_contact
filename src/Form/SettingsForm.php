<?php

namespace Drupal\entity_contact\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Manage settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'entity_contact_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_contact.settings');

    $form['entity_contact_message_store_ip_address'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Store IP addresses with form submissions'),
      '#description' => $this->t('When checked, the IP address of the form submitter will be stored with the submission. Please be aware of the applicable GDPR rules.'),
      '#default_value' => $config->get('entity_contact_message_store_ip_address'),
    ];

    $form['flood'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Flood control settings'),
      'limit' => [
        '#type' => 'number',
        '#title' => $this->t('Limit'),
        '#default_value' => $config->get('flood')['limit'],
        '#min' => 0,
      ],
      'interval' => [
        '#type' => 'number',
        '#title' => $this->t('Interval'),
        '#default_value' => $config->get('flood')['interval'],
        '#min' => 0,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_contact.settings');
    $config->set('entity_contact_message_store_ip_address', $form_state->getValue('entity_contact_message_store_ip_address') == TRUE);
    $config->set('flood', [
      'limit' => $form_state->getValue('limit'),
      'interval' => $form_state->getValue('interval'),
    ]);
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_contact.settings'];
  }

}

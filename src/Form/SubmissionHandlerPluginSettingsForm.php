<?php

namespace Drupal\entity_contact\Form;

use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_contact\SubmissionHandlerPluginInterface;
use Drupal\entity_contact\SubmissionHandlerPluginManager;

/**
 * Manage remote storage settings for a contact_form.
 */
class SubmissionHandlerPluginSettingsForm extends FormBase {

  /**
   * The remote storage plugin manager.
   *
   * @var \Drupal\entity_contact\SubmissionHandlerPluginManager
   */
  protected $submissionHandlerPluginManager;

  /**
   * The plugin instance.
   *
   * @var \Drupal\entity_contact\SubmissionHandlerPluginInterface
   */
  protected $pluginInstance;

  /**
   * The contact form.
   *
   * @var \Drupal\entity_contact\EntityContactFormInterface
   */
  protected $entityContactForm;

  /**
   * Constructor.
   *
   * @param \Drupal\entity_contact\SubmissionHandlerPluginManager $submissionHandlerPluginManager
   *   The plugin manager.
   * @param \Drupal\entity_contact\SubmissionHandlerPluginInterface $pluginInstance
   *   The plugin.
   * @param \Drupal\entity_contact\EntityContactFormInterface $entityContactForm
   *   The contact form.
   */
  public function __construct(SubmissionHandlerPluginManager $submissionHandlerPluginManager, SubmissionHandlerPluginInterface $pluginInstance, EntityContactFormInterface $entityContactForm) {
    $this->submissionHandlerPluginManager = $submissionHandlerPluginManager;
    $this->pluginInstance = $pluginInstance;
    $this->entityContactForm = $entityContactForm;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'entity_contact_submission_handler_plugin_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#entity_contact_form'] = $this->entityContactForm;
    $enabled = in_array($this->pluginInstance->getId(), $this->entityContactForm->getEnabledSubmissionHandlers());

    $form['plugin_description'] = [
      '#markup' => $this->pluginInstance->getDescription(),
    ];

    $form[$this->pluginInstance->getId()] = [
      '#type' => 'container',
      'enabled' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#default_value' => $enabled,
      ],
    ];

    if (!empty($plugin_settings_form = $this->pluginInstance->getSettingsForm($this->entityContactForm, $form_state))) {
      $form[$this->pluginInstance->getId()]['settings'] = $plugin_settings_form;
    }

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $this->pluginInstance->validateSettingsForm($form['#entity_contact_form'], $form[$this->pluginInstance->getId()]['settings'], $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $enabled_submission_handlers = $this->entityContactForm->getEnabledSubmissionHandlers();
    $key = array_search($this->pluginInstance->getId(), $enabled_submission_handlers);
    $enabled = $form_state->getValue('enabled');

    if ($enabled && $key === FALSE) {
      $enabled_submission_handlers[] = $this->pluginInstance->getId();
    }
    elseif (!$enabled && $key !== FALSE) {
      unset($enabled_submission_handlers[$key]);
    }
    $this->entityContactForm->setEnabledSubmissionHandlers($enabled_submission_handlers);

    $this->pluginInstance->submitSettingsForm($form['#entity_contact_form'], $form_state);

    $this->entityContactForm->save();
    $this->messenger()->addStatus($this->t('Your settings have been saved.'));
  }

}

<?php

namespace Drupal\entity_contact;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the contact form entity type.
 */
class EntityContactFormAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    if ($operation === 'submissions') {
      return AccessResult::allowedIfHasPermissions($account, [
        'view entity contact form submissions',
        'administer entity contact form submissions',
      ], 'OR');
    }

    if ($operation == 'view') {
      return AccessResult::allowedIfHasPermission($account, 'access entity contact form');
    }
    elseif ($operation == 'delete' || $operation == 'update') {
      return AccessResult::allowedIfHasPermission($account, 'administer entity contact forms');
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}

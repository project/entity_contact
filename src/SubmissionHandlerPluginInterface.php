<?php

namespace Drupal\entity_contact;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for a submission handler.
 */
interface SubmissionHandlerPluginInterface {

  /**
   * Get the plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function getId(): string;

  /**
   * Get the plugin title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string;

  /**
   * Get the plugin description.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string;

  /**
   * Handle the message.
   *
   * @param \Drupal\entity_contact\EntityContactMessageInterface $message
   *   The contact message entity.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function handle(EntityContactMessageInterface $message): bool;

  /**
   * Get the settings form for this plugin.
   *
   * This form will be appended to the
   * \Drupal\contact_storage_remote\Form\RemoteStorageSettingsForm form.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
   *   The contact form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form elements.
   */
  public function getSettingsForm(EntityContactFormInterface $entity_contact_form, FormStateInterface $form_state): array;

  /**
   * Validate the settings form.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
   *   The contact form.
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateSettingsForm(EntityContactFormInterface $entity_contact_form, array $form, FormStateInterface $form_state): void;

  /**
   * Process the submitted settings form values.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
   *   The contact form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitSettingsForm(EntityContactFormInterface $entity_contact_form, FormStateInterface $form_state): void;

}

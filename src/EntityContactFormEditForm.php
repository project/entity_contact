<?php

namespace Drupal\entity_contact;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Render\Element\PathElement;

/**
 * Base form for contact form edit forms.
 *
 * @internal
 */
class EntityContactFormEditForm extends EntityForm implements ContainerInjectionInterface {

  use ConfigFormBaseTrait;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Constructs a new ContactFormEditForm.
   *
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator service.
   */
  public function __construct(EmailValidatorInterface $email_validator, PathValidatorInterface $path_validator) {
    $this->emailValidator = $email_validator;
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('email.validator'),
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_contact.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface $contact_form
     */
    $contact_form = $this->entity;

    if (!$contact_form->isNew() && !$contact_form->hasFields()) {
      $url = Url::fromRoute('entity.entity_contact_message.field_ui_fields', ['entity_contact_form' => $contact_form->id()]);
      $this->messenger()
        ->addWarning($this->t('This form does not have any fields attached to it. <a href="@url">You can manage the form fields here</a>.', ['@url' => $url->toString()]));
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $contact_form->label(),
      '#required' => TRUE,
      '#weight' => 1,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $contact_form->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => '\Drupal\entity_contact\Entity\EntityContactForm::load',
      ],
      '#disabled' => !$contact_form->isNew(),
      '#weight' => 2,
    ];

    $description = $contact_form->getDescription();

    $form['description'] = [
      '#type' => 'text_format',
      '#format' => $description['format'] ?? 'html',
      '#title' => $this->t('Description'),
      '#default_value' => $description['value'] ?? '',
      '#weight' => 10,
    ];

    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#default_value' => $contact_form->getMessage(),
      '#description' => $this->t('The message to display to the user after submission of this form. Leave blank for no message.'),
      '#weight' => 20,
    ];
    $form['redirect'] = [
      '#type' => 'path',
      '#title' => $this->t('Redirect path'),
      '#convert_path' => PathElement::CONVERT_NONE,
      '#default_value' => $contact_form->getRedirectPath(),
      '#description' => $this->t('Path to redirect the user to after submission of this form. For example, type "/about" to redirect to that page. Use a relative path with a slash in front.'),
      '#weight' => 30,
    ];
    $form['submit_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit button text'),
      '#maxlength' => 255,
      '#default_value' => !empty($contact_form->getSubmitButtonText()) ? $contact_form->getSubmitButtonText() : $this->t('Send'),
      '#required' => TRUE,
      '#weight' => 40,
    ];
    $form['store_submissions_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Store submissions'),
      'store_submissions' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Store submissions'),
        '#description' => $this->t('Check if you want submissions for this form to be stored in the database. Please be aware of the applicable GDPR rules.'),
        '#default_value' => $contact_form->getStoreSubmission(),
      ],
      '#weight' => 50,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    $form = parent::afterBuild($element, $form_state);
    $form['actions']['#weight'] = 1000;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $redirect_url = $form_state->getValue('redirect');
    if ($redirect_url && $this->pathValidator->isValid($redirect_url)) {
      if (mb_substr($redirect_url, 0, 1) !== '/') {
        $form_state->setErrorByName('redirect', $this->t('The path should start with /.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $contact_form = $this->entity;
    $status = $contact_form->save();

    if ($status == SAVED_UPDATED) {
      $this->messenger()
        ->addStatus($this->t('Contact form %label has been updated.', ['%label' => $contact_form->label()]));
      $this->logger('entity_contact')
        ->notice('Contact form %label has been updated.', [
          '%label' => $contact_form->label(),
        ]);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('Contact form %label has been added.', ['%label' => $contact_form->label()]));
      $this->logger('entity_contact')
        ->notice('Contact form %label has been added.', [
          '%label' => $contact_form->label(),
        ]);
    }

    $url = Url::fromRoute('entity.entity_contact_message.field_ui_fields', ['entity_contact_form' => $contact_form->id()]);
    $form_state->setRedirectUrl($url);

    return $status;
  }

}

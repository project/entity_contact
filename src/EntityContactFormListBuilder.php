<?php

namespace Drupal\entity_contact;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of contact form entities.
 */
class EntityContactFormListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['form'] = $this->t('Form');
    $header['store_submissions'] = $this->t('Stores submissions?');
    $header = $header + parent::buildHeader();

    $this->moduleHandler()
      ->alter('entity_contact_form_list_builder_header', $header);

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface $entity
     */
    $row['form'] = $entity->label();
    $row['store_submissions'] = $entity->getStoreSubmission() ? $this->t('Yes') : $this->t('no');

    $row = $row + parent::buildRow($entity);

    $this->moduleHandler()
      ->alter('entity_contact_form_list_builder_row', $row, $entity);

    return $row;
  }

}

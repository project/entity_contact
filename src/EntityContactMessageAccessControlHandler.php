<?php

namespace Drupal\entity_contact;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the message form entity type.
 */
class EntityContactMessageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermissions($account, [
          'view entity contact form submissions',
          'administer entity contact form submissions',
        ], 'OR');

      case 'update':
      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'administer entity contact form submissions');
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      'access entity contact form',
      'administer entity contact form submissions',
    ], 'OR');
  }

}

<?php

namespace Drupal\entity_contact\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Url;
use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Defines the entity contact form entity.
 *
 * @ConfigEntityType(
 *   id = "entity_contact_form",
 *   label = @Translation("Contact form"),
 *   label_collection = @Translation("Contact forms"),
 *   label_singular = @Translation("contact form"),
 *   label_plural = @Translation("contact forms"),
 *   label_count = @PluralTranslation(
 *     singular = "@count contact form",
 *     plural = "@count contact forms",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity_contact\EntityContactFormAccessControlHandler",
 *     "list_builder" = "Drupal\entity_contact\EntityContactFormListBuilder",
 *     "view_builder" = "Drupal\entity_contact\EntityContactFormViewBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_contact\EntityContactFormEditForm",
 *       "edit" = "Drupal\entity_contact\EntityContactFormEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity_contact\EntityContactRouteProvider",
 *     },
 *   },
 *   config_prefix = "form",
 *   admin_permission = "administer entity contact forms",
 *   bundle_of = "entity_contact_message",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" =
 *   "/admin/content/entity-contact/manage/{entity_contact_form}/delete",
 *     "edit-form" =
 *   "/admin/content/entity-contact/manage/{entity_contact_form}",
 *     "collection" = "/admin/content/entity-contact",
 *     "submissions" =
 *   "/admin/content/entity-contact/manage/{entity_contact_form}/submissions",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "message",
 *     "redirect",
 *     "submit_button_text",
 *     "submission_handlers",
 *     "store_submissions"
 *   }
 * )
 */
class EntityContactForm extends ConfigEntityBundleBase implements EntityContactFormInterface {

  /**
   * The form ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of the category.
   *
   * @var string
   */
  protected $label;

  /**
   * The description text of the form.
   *
   * @var string
   */
  protected $description;

  /**
   * The message displayed to user on form submission.
   *
   * @var string
   */
  protected $message;

  /**
   * The path to redirect to on form submission.
   *
   * @var string
   */
  protected $redirect;

  /**
   * The submit button text.
   *
   * @var string
   */
  protected $submit_button_text;

  /**
   * The enabled submission handlers.
   *
   * @var string[]
   */
  protected $submission_handlers = [];

  /**
   * Wheneither to store submissions.
   *
   * @var bool
   */
  protected $store_submissions = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function setMessage($message) {
    $this->message = $message;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectPath() {
    return $this->redirect;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl() {
    if ($this->redirect) {
      $url = Url::fromUserInput($this->redirect);
    }
    else {
      $url = Url::fromRoute('<front>');
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function setRedirectPath($redirect) {
    $this->redirect = $redirect;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitButtonText() {
    return $this->submit_button_text;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubmitButtonText($text) {
    $this->submit_button_text = $text;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledSubmissionHandlers() {
    return $this->submission_handlers;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabledSubmissionHandlers(array $submissionHandlers) {
    $this->submission_handlers = $submissionHandlers;
  }

  /**
   * {@inheritdoc}
   */
  public function getStoreSubmission() {
    return $this->store_submissions;
  }

  /**
   * {@inheritdoc}
   */
  public function setStoreSubmissions(bool $store) {
    $this->store_submissions = $store;
  }

  /**
   * {@inheritdoc}
   */
  public function hasFields(): bool {
    if ($this->isNew()) {
      return FALSE;
    }

    $message = EntityContactMessage::create([
      'entity_contact_form' => $this->id(),
    ]);
    foreach ($message->getFields(FALSE) as $field) {
      if ($field->getFieldDefinition() instanceof FieldConfigInterface) {
        return TRUE;
      }
    }
    return FALSE;
  }

}

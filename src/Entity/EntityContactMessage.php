<?php

namespace Drupal\entity_contact\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_contact\EntityContactMessageInterface;

/**
 * Defines the contact message entity.
 *
 * @ContentEntityType(
 *   id = "entity_contact_message",
 *   label = @Translation("Contact form submission"),
 *   label_collection = @Translation("Contact form submissions"),
 *   label_singular = @Translation("contact form submission"),
 *   label_plural = @Translation("contact form submissions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count contact form submissions",
 *     plural = "@count contact form submissions",
 *   ),
 *   bundle_label = @Translation("Contact form"),
 *   handlers = {
 *     "access" =
 *   "Drupal\entity_contact\EntityContactMessageAccessControlHandler",
 *     "list_builder" = "Drupal\entity_contact\EntityContactMessageListBuilder",
 *     "view_builder" = "Drupal\entity_contact\EntityContactMessageViewBuilder",
 *     "form" = {
 *       "edit" = "Drupal\entity_contact\EntityContactMessageForm",
 *       "default" = "Drupal\entity_contact\EntityContactMessageAddForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" =
 *   "Drupal\Core\Entity\Form\DeleteMultipleForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity_contact\EntityContactRouteProvider",
 *     },
 *     "views_data" = "Drupal\entity_contact\EntityContactMessageViewsData",
 *   },
 *   base_table = "entity_contact_message",
 *   admin_permission = "administer entity contact form submissions",
 *   entity_keys = {
 *     "bundle" = "entity_contact_form",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "id" = "id"
 *   },
 *   bundle_entity_type = "entity_contact_form",
 *   field_ui_base_route = "entity.entity_contact_form.edit_form",
 *   links = {
 *     "collection" = "/admin/content/entity-contact/submissions",
 *     "canonical" =
 *   "/admin/content/entity-contact/submissions/{entity_contact_message}",
 *     "edit-form" =
 *   "/admin/content/entity-contact/submissions/{entity_contact_message}/edit",
 *     "delete-form" =
 *   "/admin/content/entity-contact/submissions/{entity_contact_message}/delete",
 *     "delete-multiple-form" =
 *   "/admin/content/entity-contact/submissions/delete"
 *   },
 * )
 */
class EntityContactMessage extends ContentEntityBase implements EntityContactMessageInterface {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function label() {
    /**
     * @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
     */
    $date_formatter = \Drupal::service('date.formatter');
    return $this->t('Contact form submission %id on @date', [
      '%id' => $this->id(),
      '@date' => $date_formatter->format($this->getCreatedTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getContactForm() {
    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface $entity
     */
    $entity = $this->get('entity_contact_form')->entity;
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return (int) $this->get('created')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    if ($this->isNew()) {
      $request = \Drupal::request();
      $request_uri = $request->getUri();
      $referer = $request->headers->get('referer');

      $keys = [
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
        'gclid',
      ];

      if ($request->query->get('ajax_form') == 1 && !empty($referer) && filter_var($referer, FILTER_VALIDATE_URL)) {
        // Ajax request, get everything from the referrer.
        $referer_parsed = parse_url($referer);

        if (!empty($referer_parsed['query'])) {
          $referer_query = [];
          parse_str($referer_parsed['query'], $referer_query);
          foreach ($keys as $key) {
            if (!empty($referer_query[$key])) {
              $request->query->set($key, $referer_query[$key]);
            }
          }
        }
        $request_uri = $referer;
      }

      foreach ($keys as $key) {
        $value = $request->get($key, NULL);

        $this->set($key, $value);
      }

      $this->set('submission_url', $request_uri);

      if (\Drupal::config('entity_contact.settings')
        ->get('entity_contact_message_store_ip_address')) {
        $this->set('ip_address', $request->getClientIp());
      }
    }

    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['entity_contact_form']->setLabel(t('Form ID'))
      ->setDescription(t('The ID of the associated form.'));

    $fields['uuid']->setDescription(t('The message UUID.'));

    $fields['langcode']->setDescription(t('The message language code.'));

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Message ID'))
      ->setDescription(t('The message ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the message was created.'))
      ->setTranslatable(TRUE)
      ->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The user ID.'))
      ->setSetting('target_type', 'entity_contact_form')
      ->setDefaultValueCallback('entity_contact_message_default_uid');

    $fields['ip_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP address'))
      ->setDescription(t('The IP address of the submitter.'))
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['utm_source'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UTM source'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['utm_medium'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UTM medium'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['utm_campaign'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UTM campaign'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['utm_content'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UTM content'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['utm_term'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UTM term'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['gclid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('GCLID'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['submission_url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('URL'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}

<?php

namespace Drupal\entity_contact\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a SubmisionHandler annotation object.
 *
 * @package Drupal\entity_contact\Annotation
 *
 * @Annotation
 */
class EntityContactSubmissionHandler extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin title.
   *
   * @var string
   */
  public $title;

  /**
   * The plugin description.
   *
   * @var string
   */
  public $description;

}

<?php

namespace Drupal\entity_contact;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for contact message forms.
 *
 * @internal
 */
class EntityContactMessageAddForm extends EntityContactMessageForm {

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['utm_source']['#access'] = FALSE;
    $form['utm_medium']['#access'] = FALSE;
    $form['utm_campaign']['#access'] = FALSE;
    $form['utm_content']['#access'] = FALSE;
    $form['utm_term']['#access'] = FALSE;
    $form['gclid']['#access'] = FALSE;
    $form['submission_url']['#access'] = FALSE;

    /**
     * @var \Drupal\entity_contact\EntityContactMessageInterface $message
     */
    $message = $this->getEntity();
    $form['#has_fields'] = $message->getContactForm()->hasFields();

    return $form;
  }

}

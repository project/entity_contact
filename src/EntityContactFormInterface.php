<?php

namespace Drupal\entity_contact;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a contact form entity.
 */
interface EntityContactFormInterface extends ConfigEntityInterface {

  /**
   * Returns the form description text.
   *
   * @return mixed
   *   The text
   */
  public function getDescription();

  /**
   * Returns the message to be displayed to user.
   *
   * @return string
   *   A user message.
   */
  public function getMessage();

  /**
   * Returns the path for redirect.
   *
   * @return string
   *   The redirect path.
   */
  public function getRedirectPath();

  /**
   * Returns the url object for redirect path.
   *
   * Empty redirect property results a url object of front page.
   *
   * @return \Drupal\Core\Url
   *   The redirect url object.
   */
  public function getRedirectUrl();

  /**
   * Sets the form description.
   *
   * @param string $description
   *   The description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Sets the message to be displayed to the user.
   *
   * @param string $message
   *   The message to display after form is submitted.
   *
   * @return $this
   */
  public function setMessage($message);

  /**
   * Sets the redirect path.
   *
   * @param string $redirect
   *   The desired path.
   *
   * @return $this
   */
  public function setRedirectPath($redirect);

  /**
   * Get the submit button text.
   *
   * @return string
   *   The text
   */
  public function getSubmitButtonText();

  /**
   * Set the submit button text.
   *
   * @param string $text
   *   The text.
   */
  public function setSubmitButtonText($text);

  /**
   * Get the list of enabled submission handlers.
   *
   * @return string[]
   *   The enabled plugin ids.
   */
  public function getEnabledSubmissionHandlers();

  /**
   * Set the list of enabled submission handlers.
   *
   * @param string[] $submissionHandlers
   *   The enabled plugin ids.
   */
  public function setEnabledSubmissionHandlers(array $submissionHandlers);

  /**
   * Check if submissions for this form are stored.
   *
   * @return bool
   *   True or false
   */
  public function getStoreSubmission();

  /**
   * Set if submissions for this form are stored.
   *
   * @param bool $store
   *   True or false.
   */
  public function setStoreSubmissions(bool $store);

  /**
   * Check if there are fields added to this form.
   *
   * @return bool
   *   TRUE if there are fields in this form, FALSE if not.
   */
  public function hasFields(): bool;

}

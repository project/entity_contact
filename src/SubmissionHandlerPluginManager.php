<?php

namespace Drupal\entity_contact;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_contact\Annotation\EntityContactSubmissionHandler;

/**
 * The submission handler plugin manager.
 */
class SubmissionHandlerPluginManager extends DefaultPluginManager {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->subdir = 'Plugin/entity_contact/SubmissionHandler';
    $this->namespaces = $namespaces;
    $this->pluginDefinitionAnnotationName = EntityContactSubmissionHandler::class;
    $this->pluginInterface = SubmissionHandlerPluginInterface::class;
    $this->moduleHandler = $module_handler;
    $this->logger = $loggerChannelFactory->get('entity_contact');
  }

  /**
   * Run all enabled plugins for the given message entity.
   *
   * @param \Drupal\entity_contact\EntityContactMessageInterface $message
   *   The message entity.
   */
  public function handle(EntityContactMessageInterface $message) {
    $contact_form = $message->getContactForm();

    foreach ($contact_form->getEnabledSubmissionHandlers() as $enabled_submission_handler_plugin_id) {
      try {
        try {
          /**
           * @var \Drupal\entity_contact\SubmissionHandlerPluginInterface $instance
           */
          $instance = $this->createInstance($enabled_submission_handler_plugin_id);
        }
        catch (PluginNotFoundException $e) {
          // Plugin not found, skip it.
          continue;
        }

        if (!$instance->handle($message)) {
          throw new \RuntimeException('Could not handle message ' . $message->id() . ' using plugin ' . $enabled_submission_handler_plugin_id);
        }
        else {
          $this->logger->info('Handled message ' . $message->id() . ' using plugin ' . $enabled_submission_handler_plugin_id);
        }
      }
      catch (\Exception $e) {
        watchdog_exception('entity_contact', $e);
      }
    }
  }

}

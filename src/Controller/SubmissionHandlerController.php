<?php

namespace Drupal\entity_contact\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\entity_contact\Form\SubmissionHandlerPluginSettingsForm;
use Drupal\entity_contact\SubmissionHandlerPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller which handles setting routes for submission handlers.
 */
class SubmissionHandlerController extends ControllerBase {

  /**
   * The plugin instance.
   *
   * @var \Drupal\entity_contact\SubmissionHandlerPluginInterface
   */
  protected $pluginInstance;

  /**
   * The submission handler plugin manager.
   *
   * @var \Drupal\entity_contact\SubmissionHandlerPluginManager
   */
  protected $submissionHandlerPluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   * @param \Drupal\entity_contact\SubmissionHandlerPluginManager $submissionHandlerPluginManager
   *   The remote storage plugin manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(RouteMatchInterface $routeMatch, SubmissionHandlerPluginManager $submissionHandlerPluginManager) {
    if (stripos($routeMatch->getRouteName(), 'entity_contact.submission_handler.') === 0) {
      preg_match('/entity_contact\.submission_handler\.(.*)/', $routeMatch->getRouteName(), $matches);

      if (count($matches) !== 2 || empty($matches[1])) {
        throw new \RuntimeException('Invalid route name: ' . $routeMatch->getRouteName());
      }

      $this->pluginInstance = $submissionHandlerPluginManager->createInstance($matches[1]);
    }
    else {
      $this->pluginInstance = NULL;
    }
    $this->submissionHandlerPluginManager = $submissionHandlerPluginManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('plugin.manager.entity_contact.submission_handler')
    );
  }

  /**
   * Form callback.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
   *   The contact form.
   *
   * @return array
   *   The render array.
   */
  public function form(EntityContactFormInterface $entity_contact_form) {
    $form = new SubmissionHandlerPluginSettingsForm($this->submissionHandlerPluginManager, $this->pluginInstance, $entity_contact_form);
    return $this->formBuilder()->getForm($form);
  }

  /**
   * Title callback.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
   *   The contact form.
   *
   * @return string
   *   The title
   */
  public function title(EntityContactFormInterface $entity_contact_form) {
    return $this->pluginInstance->getTitle();
  }

}

<?php

namespace Drupal\entity_contact\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity_contact\EntityContactFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller to show submissions for a specific contact form.
 */
class EntityContactFormSubmissionsController extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The contact form.
   *
   * @var \Drupal\entity_contact\EntityContactFormInterface
   */
  protected $entityContactForm;

  /**
   * Constructor.
   */
  public function __construct() {
    $entity_type_manager = \Drupal::entityTypeManager();
    $this->dateFormatter = \Drupal::service('date.formatter');
    parent::__construct($entity_type_manager->getDefinition('entity_contact_message'), $entity_type_manager->getStorage('entity_contact_message'));
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->condition('entity_contact_form', $this->entityContactForm->id())
      ->accessCheck(TRUE)
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['created'] = $this->t('Created');
    $header['form'] = $this->t('Form');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
     * @var \Drupal\entity_contact\EntityContactMessageInterface $entity
     */
    $row['id'] = $entity->id();
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    $row['form'] = $entity->getContactForm()->label();

    return $row + parent::buildRow($entity);
  }

  /**
   * Show submissions for the given contact form.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
   *   The contact form.
   *
   * @return array
   *   The render array
   */
  public function submissions(EntityContactFormInterface $entity_contact_form) {
    $this->entityContactForm = $entity_contact_form;

    if (!$this->entityContactForm->getStoreSubmission()) {
      $this->messenger()
        ->addWarning($this->t('This form does not store submissions. <a href="@edit_url">You can change this setting here</a>.', ['@edit_url' => $this->entityContactForm->toUrl('edit-form')->toString()]));
    }

    return $this->render();
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $operations = [];

    if ($entity->access('view') && $entity->hasLinkTemplate('edit-form')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 10,
        'url' => $entity->toUrl('canonical'),
      ];
    }

    $operations += parent::getOperations($entity);
    return $operations;
  }

}

<?php

/**
 * @file
 * Hooks provided by the entity_contact module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the EntityContactFormListBuilder header.
 *
 * @param array $header
 *   The header cells.
 */
function hook_entity_contact_form_list_builder_header_alter(array &$header) {

}

/**
 * Alter a EntityContactFormListBuilder row.
 *
 * @param array $row
 *   The header cells.
 */
function hook_entity_contact_form_list_builder_row_alter(array &$row, \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form) {

}

/**
 * @} End of "addtogroup hooks".
 */

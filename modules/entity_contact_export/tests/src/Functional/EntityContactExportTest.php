<?php

namespace Drupal\Tests\entity_contact_export\Functional;

/**
 * Test the export functionalities.
 *
 * @group entity_contact_export
 */
class EntityContactExportTest extends EntityContactExportTestBase {

  /**
   * Test exporting of all form submissions.
   */
  public function testExportAllSubmissions(): void {
    $this->drupalGet('/admin/content/entity-contact/submissions/export');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUserWithoutExportPermission);
    $this->drupalGet('/admin/content/entity-contact/submissions/export');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/content/entity-contact/submissions/export');
    $this->assertSession()->statusCodeEquals(200);

    $this->submitForm([
      'layout' => 'all_fields',
      'format' => 'csv',
    ], 'Start export');

    // Get the download link.
    preg_match_all('/Submissions successfully exported\. <a href="(.*)"/', $this->getSession()
      ->getPage()
      ->getContent(), $matches);

    $this->assertNotEmpty($matches[1][0]);

    // Download it.
    $this->drupalGet(ltrim($matches[1][0], '/'));
    $this->assertSession()->statusCodeEquals(200);

    $csv = $this->getSession()
      ->getPage()
      ->getContent();

    $this->assertNotEmpty($csv);
    $this->assertStringContainsString($this->form1->label(), $csv);
    $this->assertStringContainsString($this->form2->label(), $csv);
    $this->assertStringContainsString($this->form1Message2->get($this->form1FieldName)
      ->getString(), $csv);

    // Downloading should only be possible once.
    $this->drupalGet(ltrim($matches[1][0], '/'));
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Test exporting submissions for a specific form.
   */
  public function testExportFormSubmissions(): void {
    $this->drupalGet('/admin/content/entity-contact/manage/' . $this->form1->id() . '/submissions/export');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUserWithoutExportPermission);
    $this->drupalGet('/admin/content/entity-contact/manage/' . $this->form1->id() . '/submissions/export');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/content/entity-contact/manage/' . $this->form1->id() . '/submissions/export');
    $this->assertSession()->statusCodeEquals(200);

    $this->submitForm([
      'layout' => 'all_fields',
      'format' => 'csv',
    ], 'Start export');

    // Get the download link.
    preg_match_all('/Submissions successfully exported\. <a href="(.*)"/', $this->getSession()
      ->getPage()
      ->getContent(), $matches);

    $this->assertNotEmpty($matches[1][0]);

    // Download it.
    $this->drupalGet(ltrim($matches[1][0], '/'));
    $this->assertSession()->statusCodeEquals(200);

    $csv = $this->getSession()
      ->getPage()
      ->getContent();

    $this->assertNotEmpty($csv);
    $this->assertStringContainsString($this->form1->label(), $csv);
    $this->assertStringNotContainsString($this->form2->label(), $csv);
    $this->assertStringContainsString($this->form1Message2->get($this->form1FieldName)
      ->getString(), $csv);

    // Downloading should only be possible once.
    $this->drupalGet(ltrim($matches[1][0], '/'));
    $this->assertSession()->statusCodeEquals(404);
  }

}

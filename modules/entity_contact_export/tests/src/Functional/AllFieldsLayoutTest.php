<?php

namespace Drupal\Tests\entity_contact_export\Functional;

use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\entity_contact_export\Plugin\entity_contact_export\Layout\AllFields;
use Drupal\entity_contact_export_test\Plugin\entity_contact_export\Format\TestFormat;

/**
 * Test the 'All fields' export layout functionalities.
 *
 * @group entity_contact_export
 */
class AllFieldsLayoutTest extends EntityContactExportTestBase {

  /**
   * Test the initialize export method.
   */
  public function testInitializeExport(): void {
    $export_file = $this->fileRepository->writeData('', self::getExportFileDirectoryPath() . '/test.csv');
    $export_context = [
      'export_file' => $export_file->id(),
      'entity_contact_form_id' => $this->form1->id(),
    ];
    $all_fields = AllFields::create($this->container, [], 'all_fields', []);
    $test_format = TestFormat::create($this->container, [], 'test', ['extension' => 'test']);

    $all_fields->initializeExport($test_format, $export_context);

    $expected = [];
    foreach ($this->form1Message1->getFields(TRUE) as $field_name => $field) {
      $expected[0][$field_name] = $field->getFieldDefinition()->getLabel();
    }

    $this->assertEquals($expected, TestFormat::$exportData);
  }

  /**
   * Test the export submission method.
   */
  public function testExportSubmission(): void {
    $export_file = $this->fileRepository->writeData('', self::getExportFileDirectoryPath() . '/test.csv');
    $export_context = [
      'export_file' => $export_file->id(),
      'entity_contact_form_id' => $this->form1->id(),
    ];
    $all_fields = AllFields::create($this->container, [], 'all_fields', []);
    $test_format = TestFormat::create($this->container, [], 'test', ['extension' => 'test']);

    $all_fields->exportSubmission($this->form1Message1, $test_format, $export_context);

    $expected = [];
    foreach ($this->form1Message1->getFields(TRUE) as $field_name => $field) {
      $expected[0][$field_name] = [];

      foreach ($field as $field_value_item) {
        $field_value_item_value = $field_value_item->getString();
        if ($field_value_item instanceof OptionsProviderInterface) {
          $all_options = $field_value_item->getPossibleOptions();
          if (isset($all_options[$field_value_item_value])) {
            $field_value_item_value = $all_options[$field_value_item_value];
          }
        }
        $expected[0][$field_name][] = $field_value_item_value;
      }
    }

    $this->assertEquals($expected, TestFormat::$exportData);
  }

}

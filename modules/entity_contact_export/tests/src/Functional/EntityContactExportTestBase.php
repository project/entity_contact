<?php

namespace Drupal\Tests\entity_contact_export\Functional;

use Drupal\Core\File\FileSystemInterface;
use Drupal\entity_contact\Entity\EntityContactForm;
use Drupal\entity_contact\Entity\EntityContactMessage;
use Drupal\entity_contact_export\ExportFileTrait;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;

/**
 * Base test class for entity_contact functional tests.
 *
 * @group entity_contact_export
 */
abstract class EntityContactExportTestBase extends BrowserTestBase {

  use FieldUiTestTrait, ExportFileTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'text',
    'entity_contact',
    'entity_contact_export',
    'entity_contact_export_test',
    'field_ui',
    'block',
    'filter',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Form storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $formStorage;

  /**
   * Message storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $messageStorage;

  /**
   * Test filter format.
   *
   * @var \Drupal\filter\Entity\FilterFormat
   */
  protected $filter;

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * Admin user without export permission.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUserWithoutExportPermission;

  /**
   * Form 1.
   *
   * @var \Drupal\entity_contact\Entity\EntityContactForm
   */
  protected $form1;

  /**
   * Form field name.
   *
   * @var string
   */
  protected $form1FieldName;

  /**
   * Form message.
   *
   * @var \Drupal\entity_contact\EntityContactMessageInterface
   */
  protected $form1Message1;

  /**
   * Form message.
   *
   * @var \Drupal\entity_contact\EntityContactMessageInterface
   */
  protected $form1Message2;

  /**
   * Form message.
   *
   * @var \Drupal\entity_contact\EntityContactMessageInterface
   */
  protected $form2Message1;

  /**
   * Form message.
   *
   * @var \Drupal\entity_contact\EntityContactMessageInterface
   */
  protected $form2Message2;

  /**
   * Form 2.
   *
   * @var \Drupal\entity_contact\Entity\EntityContactForm
   */
  protected $form2;

  /**
   * File repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->formStorage = $this->container->get('entity_type.manager')
      ->getStorage('entity_contact_form');
    $this->messageStorage = $this->container->get('entity_type.manager')
      ->getStorage('entity_contact_message');

    $this->filter = FilterFormat::create([
      'format' => 'html',
      'name' => 'html',
      'status' => TRUE,
    ]);
    $this->filter->save();

    $this->adminUser = $this->drupalCreateUser([
      'administer entity contact forms',
      'administer entity contact form settings',
      'access entity contact form',
      $this->filter->getPermissionName(),
      'administer entity_contact_message display',
      'administer entity_contact_message fields',
      'administer entity_contact_message form display',
      'export entity contact messages',
    ]);

    $this->adminUserWithoutExportPermission = $this->drupalCreateUser([
      'administer entity contact forms',
      'administer entity contact form settings',
      'access entity contact form',
      $this->filter->getPermissionName(),
      'administer entity_contact_message display',
      'administer entity_contact_message fields',
      'administer entity_contact_message form display',
    ]);

    $this->drupalLogin($this->adminUser);

    // Create test form 1.
    $this->form1 = EntityContactForm::create([
      'id' => 'form1',
      'label' => $this->randomMachineName(),
      'description' => [
        'value' => $this->randomMachineName(),
        'format' => 'html',
      ],
      'message' => $this->randomMachineName(),
      'submit_button_text' => 'Submit',
      'submission_handlers' => [],
      'store_submissions' => TRUE,
    ]);
    $this->form1->save();
    $form1_field_name = mb_strtolower($this->randomMachineName());
    $form1_field_label = $this->randomMachineName();
    $this->fieldUIAddNewField('admin/content/entity-contact/manage/' . $this->form1->id(), $form1_field_name, $form1_field_label, 'string');
    $this->form1FieldName = 'field_' . $form1_field_name;
    $field_value = $this->randomMachineName();

    // Create test form 2.
    $this->form2 = EntityContactForm::create([
      'id' => 'form2',
      'label' => $this->randomMachineName(),
      'description' => [
        'value' => $this->randomMachineName(),
        'format' => 'html',
      ],
      'message' => $this->randomMachineName(),
      'submit_button_text' => 'Submit',
      'submission_handlers' => [],
      'store_submissions' => TRUE,
    ]);
    $this->form2->save();

    $this->form1Message1 = EntityContactMessage::create([
      'entity_contact_form' => $this->form1->id(),
    ]);
    $this->form1Message1->save();

    $this->form1Message2 = EntityContactMessage::create([
      'entity_contact_form' => $this->form1->id(),
      $this->form1FieldName => $field_value,
    ]);
    $this->form1Message2->save();

    $this->form2Message1 = EntityContactMessage::create([
      'entity_contact_form' => $this->form2->id(),
    ]);
    $this->form2Message1->save();

    $this->form2Message2 = EntityContactMessage::create([
      'entity_contact_form' => $this->form2->id(),
    ]);
    $this->form2Message2->save();

    $this->drupalLogout();

    /**
     * @var \Drupal\Core\File\FileSystemInterface $file_system
     */
    $file_system = $this->container->get('file_system');
    $path = self::getExportFileDirectoryPath();
    $file_system->prepareDirectory($path, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $this->fileRepository = $this->container->get('file.repository');
  }

}

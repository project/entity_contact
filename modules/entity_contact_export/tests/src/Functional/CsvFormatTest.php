<?php

namespace Drupal\Tests\entity_contact_export\Functional;

use Drupal\entity_contact_export\Plugin\entity_contact_export\Format\Csv;

/**
 * Test the CSV export format functionalities.
 *
 * @group entity_contact_export
 */
class CsvFormatTest extends EntityContactExportTestBase {

  /**
   * Test the CSV format exporter.
   */
  public function testExport(): void {
    $export_file = $this->fileRepository->writeData('', self::getExportFileDirectoryPath() . '/test.csv');
    $export_context = [
      'export_file' => $export_file->id(),
    ];

    $csv = Csv::create($this->container, [], 'csv', ['extension' => 'csv']);
    $csv->export(['a', 'b', 'c test', ['d1', 'd2']], $export_context);
    $csv->export([1, 2, 3], $export_context);

    $csv_data = "a,b,\"c test\",\"d1\nd2\"\n1,2,3\n";
    $this->assertEquals($csv_data, file_get_contents($export_file->getFileUri()));
  }

}

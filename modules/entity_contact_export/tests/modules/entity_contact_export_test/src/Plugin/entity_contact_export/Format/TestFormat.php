<?php

namespace Drupal\entity_contact_export_test\Plugin\entity_contact_export\Format;

use Drupal\entity_contact_export\FormatPluginBase;

/**
 * The 'test' export format.
 *
 * @EntityContactExportFormat(
 *   id = "test",
 *   title = @Translation("Test"),
 *   description = @Translation("Exports to a static variable."),
 *   extension = "csv"
 * )
 */
class TestFormat extends FormatPluginBase {

  /**
   * The exported data.
   *
   * @var array[]
   */
  public static $exportData = [];

  /**
   * {@inheritDoc}
   */
  public function export(array $fields, array $exportContext): void {
    self::$exportData[] = $fields;
  }

}

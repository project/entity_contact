<?php

namespace Drupal\entity_contact_export\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\entity_contact_export\Batch\ExportSubmissionsBatch;
use Drupal\entity_contact_export\FormatPluginManager;
use Drupal\entity_contact_export\LayoutPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to start a submissions export.
 */
class ExportSubmissionsForm extends FormBase {

  /**
   * Export layout plugin manager.
   *
   * @var \Drupal\entity_contact_export\LayoutPluginManager
   */
  protected $layoutPluginManager;

  /**
   * Export format plugin manager.
   *
   * @var \Drupal\entity_contact_export\FormatPluginManager
   */
  protected $formatPluginManager;

  /**
   * Entity contact message storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityContactMessageStorage;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructor.
   *
   * @param \Drupal\entity_contact_export\LayoutPluginManager $layoutPluginManager
   *   Export layout plugin manager.
   * @param \Drupal\entity_contact_export\FormatPluginManager $formatPluginManager
   *   Export format plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(LayoutPluginManager $layoutPluginManager, FormatPluginManager $formatPluginManager, EntityTypeManagerInterface $entityTypeManager, TimeInterface $time) {
    $this->layoutPluginManager = $layoutPluginManager;
    $this->formatPluginManager = $formatPluginManager;
    $this->entityContactMessageStorage = $entityTypeManager->getStorage('entity_contact_message');
    $this->time = $time;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entity_contact_export.layout'),
      $container->get('plugin.manager.entity_contact_export.format'),
      $container->get('entity_type.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'entity_contact_export';
  }

  /**
   * Form title callback.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface|null $entity_contact_form
   *   The entity contact form to be exported.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The tittle.
   */
  public function titleCallback(EntityContactFormInterface $entity_contact_form = NULL) {
    if ($entity_contact_form === NULL) {
      return $this->t('Export submissions');
    }

    return $this->t('Export submissions for <em>@form</em>', ['@form' => $entity_contact_form->label()]);
  }

  /**
   * Get the cookie name in which the last settings are stored.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface|null $entity_contact_form
   *   The applicable entity contact form.
   *
   * @return string
   *   The cookie name.
   */
  protected function getLastSettingsCookieKey(EntityContactFormInterface $entity_contact_form = NULL): string {
    $last_settings_cookie_key = 'entity_contact_export_last_settings';
    if ($entity_contact_form !== NULL) {
      $last_settings_cookie_key .= '_' . $entity_contact_form->id();
    }
    return $last_settings_cookie_key;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityContactFormInterface $entity_contact_form = NULL) {
    $layout_options = [];
    $not_applicable_layout_options = [];
    $last_settings_cookie_key = $this->getLastSettingsCookieKey($entity_contact_form);
    $last_settings = json_decode($this->getRequest()->cookies->get($last_settings_cookie_key, '[]'), TRUE) ?? [];

    foreach ($this->layoutPluginManager->getDefinitions() as $id => $definition) {
      /**
       * @var \Drupal\entity_contact_export\LayoutPluginInterface $plugin
       */
      $plugin = $this->layoutPluginManager->createInstance($id);
      if ($plugin->isApplicable($entity_contact_form)) {
        $layout_options[$id] = $definition['title'] . '<br><small>' . $definition['description'] . '</small>';
      }
      else {
        $not_applicable_layout_options[$id] = $definition['title'] . '<br><small>' . $definition['description'] . '</small>';
      }
    }

    $format_options = [];
    $not_applicable_format_options = [];
    foreach ($this->formatPluginManager->getDefinitions() as $id => $definition) {
      /**
       * @var \Drupal\entity_contact_export\FormatPluginInterface $plugin
       */
      $plugin = $this->formatPluginManager->createInstance($id);
      if ($plugin->isApplicable($entity_contact_form)) {
        $format_options[$id] = $definition['title'] . '<br><small>' . $definition['description'] . '</small>';
      }
      else {
        $not_applicable_format_options[$id] = $definition['title'] . '<br><small>' . $definition['description'] . '</small>';
      }
    }

    asort($layout_options);
    asort($not_applicable_layout_options);
    asort($format_options);
    asort($not_applicable_format_options);

    $form['#entity_contact_form'] = $entity_contact_form;

    $form['layout'] = [
      '#type' => 'radios',
      '#title' => $this->t('Export layout'),
      '#options' => $layout_options,
      '#required' => TRUE,
      '#default_value' => $last_settings['layout'] ?? (!empty($layout_options) ? array_keys($layout_options)[0] : NULL),
    ];

    if (!empty($not_applicable_layout_options)) {
      $form['layout_not_applicable'] = [
        '#type' => 'radios',
        '#options' => $not_applicable_layout_options,
        '#disabled' => TRUE,
      ];
    }

    $form['format'] = [
      '#type' => 'radios',
      '#title' => $this->t('Export format'),
      '#options' => $format_options,
      '#required' => TRUE,
      '#default_value' => $last_settings['format'] ?? (!empty($format_options) ? array_keys($format_options)[0] : NULL),
    ];

    if (!empty($not_applicable_format_options)) {
      $form['format_not_applicable'] = [
        '#type' => 'radios',
        '#options' => $not_applicable_format_options,
        '#disabled' => TRUE,
      ];
    }

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start export'),
      '#button_type' => 'primary',
    ];

    $form['#cache']['contexts'][] = 'cookies';

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface|null $entity_contact_form
     */
    $entity_contact_form = $form['#entity_contact_form'];

    $last_settings_cookie_key = $this->getLastSettingsCookieKey($entity_contact_form);

    $this->getRequest()->cookies->set($last_settings_cookie_key, json_encode([
      'layout' => $form_state->getValue('layout'),
      'format' => $form_state->getValue('format'),
    ]));
    $batch_builder = new BatchBuilder();

    $batch_builder->setTitle($this->t('Exporting submissions'));
    $batch_builder->setInitMessage($this->t('Starting export'));
    $batch_builder->setProgressMessage($this->t('Exporting submissions'));
    $batch_builder->setErrorMessage($this->t('An error occurred while exporting'));
    $batch_builder->setFinishCallback([
      ExportSubmissionsBatch::class,
      'finished',
    ]);

    $batch_builder->addOperation([
      ExportSubmissionsBatch::class,
      'initialize',
    ], [
      $entity_contact_form !== NULL ? $entity_contact_form->id() : NULL,
      $form_state->getValue('layout'),
      $form_state->getValue('format'),
      $this->time->getRequestTime(),
    ]);

    $query = $this->entityContactMessageStorage->getQuery();
    $query->accessCheck(TRUE);
    if ($entity_contact_form !== NULL) {
      $query->condition('entity_contact_form', $entity_contact_form->id());
    }

    foreach ($query->execute() as $entity_contact_message_id) {
      $batch_builder->addOperation([
        ExportSubmissionsBatch::class,
        'exportSingleSubmission',
      ], [
        $entity_contact_message_id,
      ]);
    }

    batch_set($batch_builder->toArray());
  }

}

<?php

namespace Drupal\entity_contact_export\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Session\AccountInterface;

/**
 * Access checker for the export routes.
 */
class ExportAccessCheck implements AccessInterface {

  /**
   * Determine if we have access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   * @param \Drupal\Core\Routing\RouteMatch $routeMatch
   *   The current route match.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
   *   The result.
   */
  public function access(AccountInterface $account, RouteMatch $routeMatch) {
    /**
     * @var \Drupal\entity_contact\EntityContactFormInterface|null $entity_contact_form
     */
    $entity_contact_form = $routeMatch->getParameter('entity_contact_form');

    if ($entity_contact_form !== NULL && ($entity_contact_form->isNew() || $entity_contact_form->getStoreSubmission() === FALSE)) {
      return AccessResult::forbidden()
        ->addCacheableDependency($entity_contact_form);
    }

    return AccessResult::allowedIfHasPermissions($account, [
      'view entity contact form submissions',
      'export entity contact messages',
    ], 'OR');
  }

}

<?php

namespace Drupal\entity_contact_export\Plugin\entity_contact_export\Format;

use Drupal\entity_contact_export\FormatPluginBase;

/**
 * The 'all fields' export format.
 *
 * @EntityContactExportFormat(
 *   id = "csv",
 *   title = @Translation("CSV"),
 *   description = @Translation("Exports to a CSV file."),
 *   extension = "csv"
 * )
 */
class Csv extends FormatPluginBase {

  /**
   * {@inheritDoc}
   */
  public function export(array $fields, array $exportContext): void {
    $path = self::getExportFileRealPath($exportContext);

    $handle = fopen($path, 'a');
    if (!$handle) {
      throw new \RuntimeException('Could not open ' . $path . ' for writing.');
    }

    foreach ($fields as $key => $value) {
      if (is_array($value)) {
        $fields[$key] = implode("\n", $value);
      }
    }

    if (fputcsv($handle, $fields) === FALSE) {
      throw new \RuntimeException('Could not write fields to ' . $path);
    }

    fclose($handle);
  }

}

<?php

namespace Drupal\entity_contact_export\Plugin\entity_contact_export\Layout;

use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\entity_contact\EntityContactMessageInterface;
use Drupal\entity_contact_export\FormatPluginInterface;
use Drupal\entity_contact_export\LayoutPluginBase;

/**
 * The 'all fields' export layout.
 *
 * @EntityContactExportLayout(
 *   id = "all_fields",
 *   title = @Translation("All fields"),
 *   description = @Translation("Exports all fields added to the form."),
 * )
 */
class AllFields extends LayoutPluginBase {

  /**
   * {@inheritDoc}
   */
  public function exportSubmission(EntityContactMessageInterface $message, FormatPluginInterface $formatPlugin, array $exportContext): void {
    $fields = [];

    foreach ($message->getFields(TRUE) as $field_name => $field) {
      $fields[$field_name] = [];

      foreach ($field as $field_value_item) {
        $field_value_item_value = $field_value_item->getString();
        if ($field_value_item instanceof OptionsProviderInterface) {
          $all_options = $field_value_item->getPossibleOptions();
          if (isset($all_options[$field_value_item_value])) {
            $field_value_item_value = $all_options[$field_value_item_value];
          }
        }
        $fields[$field_name][] = $field_value_item_value;
      }
    }

    $formatPlugin->export($fields, $exportContext);
  }

}

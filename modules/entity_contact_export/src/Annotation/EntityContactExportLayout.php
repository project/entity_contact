<?php

namespace Drupal\entity_contact_export\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an entity_contact_export layout annotation object.
 *
 * @Annotation
 */
class EntityContactExportLayout extends Plugin {

  /**
   * The layout id.
   *
   * @var int
   */
  public $id;

  /**
   * The layout title.
   *
   * @var string
   */
  public $title;

  /**
   * The layout description.
   *
   * @var string
   */
  public $description;

}

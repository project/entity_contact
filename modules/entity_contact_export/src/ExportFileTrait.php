<?php

namespace Drupal\entity_contact_export;

use Drupal\file\FileInterface;

/**
 * Export file helper methods.
 */
trait ExportFileTrait {

  /**
   * Get the export file.
   *
   * @param array $exportContext
   *   The export context.
   *
   * @return \Drupal\file\FileInterface
   *   The export file.
   */
  protected static function getExportFile(array $exportContext): FileInterface {
    $file = \Drupal::entityTypeManager()
      ->getStorage('file')
      ->load($exportContext['export_file']);

    if (!$file instanceof FileInterface) {
      throw new \RuntimeException('File is missing ' . $exportContext['export_file']);
    }

    if (self::isExportFile($file) === FALSE) {
      throw new \RuntimeException('File is not an export file: ' . $exportContext['export_file']);
    }
    return $file;
  }

  /**
   * Get the filesystem path to the export file.
   *
   * @param array $exportContext
   *   The export context.
   *
   * @return string
   *   The export file path.
   */
  protected static function getExportFileRealPath(array $exportContext): string {
    $file = self::getExportFile($exportContext);

    return \Drupal::service('file_system')->realpath($file->getFileUri());
  }

  /**
   * Check if the given file is an export file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  protected static function isExportFile(FileInterface $file): bool {
    return strpos($file->getFileUri(), self::getExportFileDirectoryPath() . '/') === 0;
  }

  /**
   * Get the path to the directory where export files are stored.
   *
   * @return string
   *   The path.
   */
  protected static function getExportFileDirectoryPath(): string {
    return self::getExportFilePathScheme() . '://entity_contact_export';
  }

  /**
   * Get the scheme where export files are stored.
   *
   * @return string
   *   The scheme.
   */
  protected static function getExportFilePathScheme(): string {
    return 'private';
  }

}

<?php

namespace Drupal\entity_contact_export;

use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * The eveent subscriber which deletes export files after they are downloaded.
 */
class EventSubscriber implements EventSubscriberInterface {

  use ExportFileTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   *   The current user.
   */
  public function __construct(AccountProxyInterface $accountProxy) {
    $this->accountProxy = $accountProxy;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onkernelResponse'];
    return $events;
  }

  /**
   * The onkernelResponse event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event.
   */
  public function onkernelResponse(ResponseEvent $event) {
    foreach ($event->getRequest()->cookies->all() as $key => $value) {
      if (strpos($key, 'entity_contact_export_last_settings') === 0) {
        // Transfer the cookies from the request to the response to make this
        // work with the drupal batch.
        $event->getResponse()->headers->setCookie(Cookie::create($key, $value, time() + 60 * 60 * 24 * 365, '/', NULL, TRUE, TRUE));
      }
    }
  }

}

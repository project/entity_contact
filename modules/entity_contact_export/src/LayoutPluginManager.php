<?php

namespace Drupal\entity_contact_export;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_contact_export\Annotation\EntityContactExportLayout;

/**
 * The export layout plugin manager.
 */
class LayoutPluginManager extends DefaultPluginManager {

  /**
   * {@inheritDoc}
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    $this->subdir = 'Plugin/entity_contact_export/Layout';
    $this->namespaces = $namespaces;
    $this->pluginDefinitionAnnotationName = EntityContactExportLayout::class;
    $this->pluginInterface = LayoutPluginInterface::class;
    $this->moduleHandler = $module_handler;
  }

}

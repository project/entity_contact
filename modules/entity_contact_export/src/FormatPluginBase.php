<?php

namespace Drupal\entity_contact_export;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\entity_contact\EntityContactFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for entity_contact_export format plugins.
 */
abstract class FormatPluginBase extends PluginBase implements FormatPluginInterface, ContainerFactoryPluginInterface {

  use ExportFileTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Contact form storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityContactFormStorage;

  /**
   * Contact message storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityContactMessageStorage;

  /**
   * File storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityContactFormStorage = $entityTypeManager->getStorage('entity_contact_form');
    $this->entityContactMessageStorage = $entityTypeManager->getStorage('entity_contact_message');
    $this->fileStorage = $entityTypeManager->getStorage('file');
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getExtension(array $exportContext): string {
    return $this->pluginDefinition['extension'];
  }

  /**
   * {@inheritDoc}
   */
  public function initializeExport(array $exportContext): void {
  }

  /**
   * {@inheritDoc}
   */
  public function finalizeExport(array $exportContext): void {
  }

  /**
   * {@inheritDoc}
   */
  public function isApplicable(EntityContactFormInterface $entityContactForm = NULL): bool {
    return TRUE;
  }

}

<?php

namespace Drupal\entity_contact_export;

use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\entity_contact\EntityContactMessageInterface;

/**
 * Interface for entity_contact_export layout plugins.
 */
interface LayoutPluginInterface {

  /**
   * Initialize the export.
   *
   * @param \Drupal\entity_contact_export\FormatPluginInterface $formatPlugin
   *   The format plugin to export to.
   * @param array $exportContext
   *   The export context.
   */
  public function initializeExport(FormatPluginInterface $formatPlugin, array $exportContext): void;

  /**
   * Finalize the export.
   *
   * @param \Drupal\entity_contact_export\FormatPluginInterface $formatPlugin
   *   The format plugin to export to.
   * @param array $exportContext
   *   The export context.
   */
  public function finalizeExport(FormatPluginInterface $formatPlugin, array $exportContext): void;

  /**
   * Export the given submission to an array of fields.
   *
   * @param \Drupal\entity_contact\EntityContactMessageInterface $message
   *   The message.
   * @param \Drupal\entity_contact_export\FormatPluginInterface $formatPlugin
   *   The format to export to.
   * @param array $exportContext
   *   The export context.
   */
  public function exportSubmission(EntityContactMessageInterface $message, FormatPluginInterface $formatPlugin, array $exportContext): void;

  /**
   * Check if this layout can be used for the given contact form.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface|null $entityContactForm
   *   The contact form.
   *
   * @return bool
   *   TRUE or FALSE
   */
  public function isApplicable(EntityContactFormInterface $entityContactForm = NULL): bool;

}

<?php

namespace Drupal\entity_contact_export;

use Drupal\entity_contact\EntityContactFormInterface;

/**
 * Interface for entity_contact_export format plugins.
 */
interface FormatPluginInterface {

  /**
   * Get the export file extension.
   *
   * @param array $exportContext
   *   The export context.
   *
   * @return string
   *   The extension.
   */
  public function getExtension(array $exportContext): string;

  /**
   * Initialize the export.
   *
   * @param array $exportContext
   *   The export context.
   */
  public function initializeExport(array $exportContext): void;

  /**
   * Finalize the export.
   *
   * @param array $exportContext
   *   The export context.
   */
  public function finalizeExport(array $exportContext): void;

  /**
   * Export the given fields.
   *
   * @param array $fields
   *   The fields to export.
   * @param array $exportContext
   *   The export context.
   */
  public function export(array $fields, array $exportContext): void;

  /**
   * Check if this format can be used for the given contact form.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface|null $entityContactForm
   *   The contact form.
   *
   * @return bool
   *   TRUE or FALSE
   */
  public function isApplicable(EntityContactFormInterface $entityContactForm = NULL): bool;

}

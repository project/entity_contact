<?php

namespace Drupal\entity_contact_export;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_contact_export\Annotation\EntityContactExportFormat;

/**
 * The export format plugin manager.
 */
class FormatPluginManager extends DefaultPluginManager {

  /**
   * {@inheritDoc}
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    $this->subdir = 'Plugin/entity_contact_export/Format';
    $this->namespaces = $namespaces;
    $this->pluginDefinitionAnnotationName = EntityContactExportFormat::class;
    $this->pluginInterface = FormatPluginInterface::class;
    $this->moduleHandler = $module_handler;
  }

}

<?php

namespace Drupal\entity_contact_export;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\entity_contact\Entity\EntityContactForm;
use Drupal\entity_contact\Entity\EntityContactMessage;
use Drupal\entity_contact\EntityContactFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for entity_contact_export layout plugins.
 */
abstract class LayoutPluginBase extends PluginBase implements LayoutPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Contact form storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityContactFormStorage;

  /**
   * Contact message storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityContactMessageStorage;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, DateFormatterInterface $dateFormatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityContactFormStorage = $entityTypeManager->getStorage('entity_contact_form');
    $this->entityContactMessageStorage = $entityTypeManager->getStorage('entity_contact_message');
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * Get the header fields.
   *
   * @param array $exportContext
   *   The export context.
   *
   * @return array
   *   The header fields.
   */
  protected function getHeaderFields(array $exportContext): array {
    if (empty($exportContext['entity_contact_form_id'])) {
      // Create a dummy contact form to retrieve all default form fields.
      $dummy_form = EntityContactForm::create([
        'id' => uniqid(),
      ]);
      $message = EntityContactMessage::create(['entity_contact_form' => $dummy_form->id()]);
    }
    else {
      $entity_contact_form = $this->entityContactFormStorage->load($exportContext['entity_contact_form_id']);
      $message = EntityContactMessage::create(['entity_contact_form' => $entity_contact_form->id()]);
    }

    $header_fields = [];

    foreach ($message->getFields(TRUE) as $field_name => $field) {
      $header_fields[$field_name] = $field->getFieldDefinition()->getLabel();
    }

    return $header_fields;
  }

  /**
   * {@inheritDoc}
   */
  public function initializeExport(FormatPluginInterface $formatPlugin, array $exportContext): void {
    $formatPlugin->export($this->getHeaderFields($exportContext), $exportContext);
  }

  /**
   * {@inheritDoc}
   */
  public function finalizeExport(FormatPluginInterface $formatPlugin, array $exportContext): void {
  }

  /**
   * {@inheritDoc}
   */
  public function isApplicable(EntityContactFormInterface $entityContactForm = NULL): bool {
    return TRUE;
  }

}

<?php

namespace Drupal\entity_contact_export\Batch;

use Drupal\Core\File\FileSystemInterface;
use Drupal\entity_contact\EntityContactMessageInterface;
use Drupal\entity_contact_export\ExportFileTrait;
use Drupal\entity_contact_export\FormatPluginInterface;
use Drupal\entity_contact_export\LayoutPluginInterface;

/**
 * Batch functions for exporting submissions.
 */
class ExportSubmissionsBatch {

  use ExportFileTrait;

  /**
   * Get the layout plugin instance.
   *
   * @param array $exportContext
   *   The export context.
   *
   * @return \Drupal\entity_contact_export\LayoutPluginInterface
   *   The layout plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected static function getLayoutPlugin(array $exportContext): LayoutPluginInterface {
    /**
     * @var \Drupal\entity_contact_export\LayoutPluginManager $export_layout_manager
     */
    $export_layout_manager = \Drupal::service('plugin.manager.entity_contact_export.layout');

    /**
     * @var \Drupal\entity_contact_export\LayoutPluginInterface $layout_plugin
     */
    $layout_plugin = $export_layout_manager->createInstance($exportContext['layout']);

    return $layout_plugin;
  }

  /**
   * Get the format plugin instance.
   *
   * @param array $exportContext
   *   The export context.
   *
   * @return \Drupal\entity_contact_export\FormatPluginInterface
   *   The format plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected static function getFormatPlugin(array $exportContext): FormatPluginInterface {
    /**
     * @var \Drupal\entity_contact_export\FormatPluginManager $export_format_manager
     */
    $export_format_manager = \Drupal::service('plugin.manager.entity_contact_export.format');

    /**
     * @var \Drupal\entity_contact_export\FormatPluginInterface $format_plugin
     */
    $format_plugin = $export_format_manager->createInstance($exportContext['format']);

    return $format_plugin;
  }

  /**
   * Initialize the export.
   *
   * @param string $entity_contact_form_id
   *   The entity contact form id that is being exported, or null if all forms.
   * @param string $layout
   *   The layout used for the export.
   * @param string $format
   *   The format used for the export.
   * @param int $startTime
   *   The time the export was started.
   * @param array $context
   *   The batch context.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function initialize($entity_contact_form_id, string $layout, string $format, int $startTime, array &$context) {
    $context['results']['export_context'] = [
      'entity_contact_form_id' => $entity_contact_form_id,
      'layout' => $layout,
      'format' => $format,
      'start_time' => $startTime,
    ];

    if (!empty($entity_contact_form_id)) {
      $entity_contact_form = \Drupal::entityTypeManager()
        ->getStorage('entity_contact_form')
        ->load($entity_contact_form_id);
    }
    else {
      $entity_contact_form = NULL;
    }

    $layout_plugin = self::getLayoutPlugin($context['results']['export_context']);
    $format_plugin = self::getFormatPlugin($context['results']['export_context']);

    if (!$layout_plugin->isApplicable($entity_contact_form)) {
      throw new \RuntimeException('Layout ' . $layout . ' is not applicable for form ' . ($entity_contact_form !== NULL ? $entity_contact_form->id() : 'null'));
    }

    if (!$format_plugin->isApplicable($entity_contact_form)) {
      throw new \RuntimeException('Format ' . $format . ' is not applicable for form ' . ($entity_contact_form !== NULL ? $entity_contact_form->id() : 'null'));
    }

    /**
     * @var \Drupal\Core\File\FileSystemInterface $file_system
     */
    $file_system = \Drupal::service('file_system');

    $export_file_directory_path = self::getExportFileDirectoryPath();
    $file_system->prepareDirectory($export_file_directory_path, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $export_file_path = $export_file_directory_path . '/' . uniqid() . '.' . $format_plugin->getExtension($context['results']['export_context']);

    /**
     * @var \Drupal\file\FileRepositoryInterface $file_repository
     */
    $file_repository = \Drupal::service('file.repository');

    $file = $file_repository->writeData('', $export_file_path);
    $file->set('status', 0);
    $file->save();
    $context['results']['export_context']['export_file'] = $file->id();

    $format_plugin->initializeExport($context['results']['export_context']);
    $layout_plugin->initializeExport($format_plugin, $context['results']['export_context']);
  }

  /**
   * Export a single submission.
   *
   * @param string $entity_contact_message_id
   *   The entity contact message id.
   * @param array $context
   *   The batch context.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function exportSingleSubmission(string $entity_contact_message_id, array &$context) {
    $entity_contact_message = \Drupal::entityTypeManager()
      ->getStorage('entity_contact_message')
      ->load($entity_contact_message_id);

    if (!$entity_contact_message instanceof EntityContactMessageInterface) {
      return;
    }

    $layout_plugin = self::getLayoutPlugin($context['results']['export_context']);
    $format_plugin = self::getFormatPlugin($context['results']['export_context']);
    $layout_plugin->exportSubmission($entity_contact_message, $format_plugin, $context['results']['export_context']);
  }

  /**
   * Export batch finished callback.
   *
   * @param bool $success
   *   Success or not?
   * @param array $results
   *   The results.
   * @param array $operations
   *   Any unprocessed operations.
   */
  public static function finished(bool $success, array $results, array $operations) {
    $layout_plugin = self::getLayoutPlugin($results['export_context']);
    $format_plugin = self::getFormatPlugin($results['export_context']);
    $file = self::getExportFile($results['export_context']);

    if ($success) {
      $layout_plugin->finalizeExport($format_plugin, $results['export_context']);
      $format_plugin->finalizeExport($results['export_context']);

      $url = \Drupal::service('file_url_generator')
        ->generateString($file->getFileUri());
      \Drupal::messenger()
        ->addMessage(t('Submissions successfully exported. <a href="@url">Click here to download</a>', ['@url' => $url]));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      \Drupal::messenger()->addMessage($message, 'error');
    }
  }

}

<?php

namespace Drupal\entity_contact_export_xlsx\Plugin\entity_contact_export\Format;

use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\entity_contact_export\FormatPluginBase;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * The 'Excel' export format.
 *
 * @EntityContactExportFormat(
 *   id = "xlsx",
 *   title = @Translation("Excel"),
 *   description = @Translation("Exports to a Microsoft Excel (xlsx) file."),
 *   extension = "xlsx"
 * )
 */
class Xlsx extends FormatPluginBase {

  /**
   * {@inheritDoc}
   */
  public function export(array $fields, array $exportContext): void {
    $path = self::getExportFileRealPath($exportContext);
    clearstatcache($path);
    if (filesize($path) === 0) {
      // Create file.
      $spreadsheet = new Spreadsheet();
      $active_sheet = $spreadsheet->getActiveSheet();
      $highest_row = 0;
    }
    else {
      // Append.
      $spreadsheet = IOFactory::load($path, 0, [IOFactory::READER_XLSX]);
      $active_sheet = $spreadsheet->getActiveSheet();
      $highest_row = $active_sheet->getHighestRow();
    }

    $column_index = 1;
    foreach ($fields as $field) {
      if (is_array($field)) {
        $value = implode("\n", $field);
      }
      else {
        $value = $field;
      }
      $active_sheet->setCellValue([$column_index, $highest_row + 1], $value);
      $column_index++;
    }

    $writer = IOFactory::createWriter($spreadsheet, IOFactory::WRITER_XLSX);
    $writer->save($path);
  }

  /**
   * {@inheritDoc}
   */
  public function isApplicable(EntityContactFormInterface $entityContactForm = NULL): bool {
    return entity_contact_export_xlsx_has_phpspreadsheet();
  }

}

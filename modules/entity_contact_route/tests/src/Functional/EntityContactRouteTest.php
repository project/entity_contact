<?php

namespace Drupal\Tests\entity_contact_route\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;

/**
 * Test the entity_contact_route module.
 *
 * @group entity_contact_route
 */
class EntityContactRouteTest extends BrowserTestBase {

  use FieldUiTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'text',
    'entity_contact',
    'entity_contact_test',
    'entity_contact_route',
    'field_ui',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');
  }

  /**
   * Test the contact form route.
   */
  public function testContactFormRoute(): void {
    $this->drupalGet('contact-form/feedback');
    $this->assertSession()->statusCodeEquals(403);

    // With access entity contact form permission.
    user_role_grant_permissions('anonymous', ['access entity contact form']);
    $this->drupalGet('contact-form/feedback');
    $this->assertSession()->statusCodeEquals(200);
  }

}

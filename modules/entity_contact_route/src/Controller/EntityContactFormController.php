<?php

namespace Drupal\entity_contact_route\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\entity_contact\EntityContactFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for contact routes.
 */
class EntityContactFormController extends ControllerBase {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a ContactController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Build the contact form.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
   *   The contact form.
   *
   * @return array
   *   The form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function build(EntityContactFormInterface $entity_contact_form) {
    $message = $this->entityTypeManager()
      ->getStorage('entity_contact_message')
      ->create([
        'entity_contact_form' => $entity_contact_form->id(),
      ]);

    $form = $this->entityFormBuilder()->getForm($message);
    $form['#title'] = $entity_contact_form->label();

    return [
      '#theme' => 'entity_contact_route',
      '#form' => $form,
      '#entity_contact_form' => $entity_contact_form,
    ];
  }

  /**
   * Get the contact form title.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity_contact_form
   *   The contact form.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string|null
   *   The title.
   */
  public function title(EntityContactFormInterface $entity_contact_form) {
    return $entity_contact_form->label();
  }

}

<?php

namespace Drupal\entity_contact_email\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\entity_contact\Entity\EntityContactForm;
use Drupal\entity_contact\Entity\EntityContactMessage;
use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\entity_contact_email\EntityContactEmailInterface;

/**
 * Defines the entity contact form entity.
 *
 * @ConfigEntityType(
 *   id = "entity_contact_email",
 *   label = @Translation("Contact form e-mail"),
 *   label_collection = @Translation("Contact form e-mails"),
 *   label_singular = @Translation("contact form email"),
 *   label_plural = @Translation("contact form emails"),
 *   label_count = @PluralTranslation(
 *     singular = "@count contact form email",
 *     plural = "@count contact form emails",
 *   ),
 *   handlers = {
 *     "access" =
 *   "Drupal\entity_contact_email\EntityContactEmailAccessControlHandler",
 *     "list_builder" =
 *   "Drupal\entity_contact_email\EntityContactEmailListBuilder",
 *     "view_builder" =
 *   "Drupal\entity_contact_email\EntityContactEmailViewBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_contact_email\EntityContactEmailEditForm",
 *       "edit" = "Drupal\entity_contact_email\EntityContactEmailEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\entity_contact_email\EntityContactEmailRouteProvider",
 *     },
 *   },
 *   config_prefix = "email",
 *   admin_permission = "administer entity contact form emails",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" =
 *   "/admin/content/entity-contact/manage/{entity_contact_form}/email/{entity_contact_email}/delete",
 *     "edit-form" =
 *   "/admin/content/entity-contact/manage/{entity_contact_form}/email/{entity_contact_email}",
 *     "collection" =
 *   "/admin/content/entity-contact/manage/{entity_contact_form}/email",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "entityContactForm",
 *     "subject",
 *     "body",
 *     "recipients"
 *   }
 * )
 */
class EntityContactEmail extends ConfigEntityBase implements EntityContactEmailInterface {

  /**
   * The form ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of the category.
   *
   * @var string
   */
  protected $label;

  /**
   * The entity contact form this is related to.
   *
   * @var string
   */
  protected $entityContactForm;

  /**
   * The subject text of the e-mail.
   *
   * @var string
   */
  protected $subject = '';

  /**
   * The body text of the e-mail.
   *
   * @var string
   */
  protected $body = [];

  /**
   * The e-mail recipients.
   *
   * @var string[]
   */
  protected $recipients = [];

  /**
   * {@inheritDoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += [
      'subject' => t('Form [entity_contact_form:label] submitted at [entity_contact_message:submission_url]'),
      'body' => [
        'value' => '[entity_contact_message:list-fields]',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityContactForm(): EntityContactFormInterface {
    return EntityContactForm::load($this->entityContactForm);
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityContactForm(EntityContactFormInterface $entityContactForm): void {
    $this->entityContactForm = $entityContactForm->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * {@inheritdoc}
   */
  public function setSubject(string $subject): void {
    $this->subject = $subject;
  }

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    return $this->body;
  }

  /**
   * {@inheritdoc}
   */
  public function setBody($body) {
    $this->body = $body;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipients(): array {
    return $this->recipients;
  }

  /**
   * {@inheritdoc}
   */
  public function setRecipients(array $recipients): void {
    $this->recipients = $recipients;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipientEmailAddresses(): array {
    /**
     * @var \Drupal\Component\Utility\EmailValidatorInterface $email_validator
     */
    $email_validator = \Drupal::service('email.validator');
    $recipients = [];
    foreach ($this->getRecipients() as $recipient) {
      if ($email_validator->isValid($recipient)) {
        $recipients[] = $recipient;
      }
    }
    return $recipients;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipientFieldNames(): array {
    $message = EntityContactMessage::create(['entity_contact_form' => $this->entityContactForm]);
    $recipients = [];
    $valid_field_names = [];
    foreach ($message->getFields(TRUE) as $field_name => $field) {
      if ($field->getFieldDefinition()->getType() === 'email') {
        $valid_field_names[] = $field_name;
      }
    }
    foreach ($this->getRecipients() as $recipient) {
      if (in_array($recipient, $valid_field_names)) {
        $recipients[] = $recipient;
      }
    }
    return $recipients;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['entity_contact_form'] = $this->getEntityContactForm()
      ->id();

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    $entity_contact_form = $this->getEntityContactForm();

    $dependencies->addDependency('config', $entity_contact_form->getConfigDependencyName());

    $field_definitions = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions('entity_contact_message', $entity_contact_form->id());

    foreach ($this->getRecipientFieldNames() as $field_name) {
      /**
       * @var \Drupal\field\FieldConfigInterface $field
       */
      $field = $field_definitions[$field_name];
      $dependencies->addDependency('config', $field->getConfigDependencyName());
    }

    return $dependencies;
  }

}

<?php

namespace Drupal\entity_contact_email;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for contact form emails.
 */
class EntityContactEmailRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $route_collection = parent::getRoutes($entity_type);

    if ($entity_type->hasLinkTemplate('collection')) {
      $route = (new Route($entity_type->getLinkTemplate('collection')))
        ->addDefaults([
          '_entity_list' => 'entity_contact_email',
          '_title' => 'E-mails',
        ])
        ->addRequirements([
          '_permission' => 'administer entity contact form emails',
        ]);
      $route_collection->add('entity.' . $entity_type->id() . '.collection', $route);
    }

    return $route_collection;
  }

}

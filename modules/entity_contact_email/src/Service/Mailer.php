<?php

namespace Drupal\entity_contact_email\Service;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Utility\Token;
use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\entity_contact\EntityContactMessageInterface;
use Drupal\entity_contact_email\EntityContactEmailInterface;

/**
 * The mailer services which sends all configured mails.
 */
class Mailer implements MailerInterface {

  /**
   * The entity_contact_email storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidator
   */
  protected $emailValidator;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The token utility.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * {@inheritDoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerChannelFactory, EmailValidatorInterface $emailValidator, MailManagerInterface $mailManager, LanguageManagerInterface $languageManager, RendererInterface $renderer, Token $token) {
    $this->storage = $entityTypeManager->getStorage('entity_contact_email');
    $this->logger = $loggerChannelFactory->get('entity_contact_email');
    $this->emailValidator = $emailValidator;
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
    $this->renderer = $renderer;
    $this->token = $token;
  }

  /**
   * {@inheritDoc}
   */
  public function sendEmailsForEntityContactMessage(EntityContactMessageInterface $message): void {
    $entity_contact_form = $message->getContactForm();

    /**
     * @var \Drupal\entity_contact_email\EntityContactEmailInterface[] $entities
     */
    $entities = $this->storage->loadByProperties([
      'entityContactForm' => $entity_contact_form->id(),
    ]);

    foreach ($entities as $entity) {
      $this->sendMail($entity, $entity_contact_form, $message);
    }
  }

  /**
   * Send a single e-mail to a single recipient.
   *
   * @param string $recipient
   *   The recipient.
   * @param \Drupal\entity_contact_email\EntityContactEmailInterface $email
   *   The e-mail.
   * @param \Drupal\entity_contact\EntityContactFormInterface $form
   *   The form.
   * @param \Drupal\entity_contact\EntityContactMessageInterface $message
   *   The message.
   */
  protected function sendMailToRecipient(string $recipient, EntityContactEmailInterface $email, EntityContactFormInterface $form, EntityContactMessageInterface $message) {
    $current_langcode = $this->languageManager->getCurrentLanguage()->getId();

    $email_subject = $this->token->replace($email->getSubject(), [
      'entity_contact_email' => $email,
      'entity_contact_message' => $message,
      'entity_contact_form' => $form,
    ]);

    $body = $email->getBody();
    $email_body = [
      '#type' => 'processed_text',
      '#text' => $body['value'] ?? NULL,
      '#format' => $body['format'] ?? NULL,
    ];

    $email_body['#text'] = $this->token->replace($email_body['#text'], [
      'entity_contact_email' => $email,
      'entity_contact_message' => $message,
      'entity_contact_form' => $form,
    ]);

    $params = [
      'entity_contact_email' => $email,
      'entity_contact_message' => $message,
      'entity_contact_form' => $form,
      'recipient' => $recipient,
      'subject' => $email_subject,
      'body' => $this->renderer->renderplain($email_body),
    ];

    $this->logger->notice('Sending e-mail ' . $email->id() . ' to ' . $recipient . ' for submission ' . $message->id());
    $this->mailManager->mail('entity_contact_email', 'entity_contact_email', $recipient, $current_langcode, $params);
  }

  /**
   * Get all recipients.
   *
   * This combines all statically entered recipients and the message
   * field values to one list of recipients.
   *
   * @param \Drupal\entity_contact_email\EntityContactEmailInterface $email
   *   The e-mail.
   * @param \Drupal\entity_contact\EntityContactMessageInterface $message
   *   The message.
   *
   * @return string[]
   *   The list of recipient e-mail addresses.
   */
  protected function getRecipientEmailAddresses(EntityContactEmailInterface $email, EntityContactMessageInterface $message): array {
    $recipients = $email->getRecipientEmailAddresses();

    foreach ($email->getRecipientFieldNames() as $field) {
      foreach ($message->get($field) as $field_value) {
        $email = $field_value->getString();
        if ($this->emailValidator->isValid($email) && !in_array($email, $recipients)) {
          $recipients[] = $email;
        }
      }
    }

    return $recipients;
  }

  /**
   * Send a single e-mail.
   *
   * @param \Drupal\entity_contact_email\EntityContactEmailInterface $email
   *   The e-mail.
   * @param \Drupal\entity_contact\EntityContactFormInterface $form
   *   The form.
   * @param \Drupal\entity_contact\EntityContactMessageInterface $message
   *   The message.
   */
  protected function sendMail(EntityContactEmailInterface $email, EntityContactFormInterface $form, EntityContactMessageInterface $message) {
    $recipients = $this->getRecipientEmailAddresses($email, $message);
    if (count($recipients) === 0) {
      $this->logger->notice($email->id() . ' has no recipients for message ' . $message->id());
      return;
    }

    foreach ($recipients as $recipient) {
      $this->sendMailToRecipient($recipient, $email, $form, $message);
    }
  }

}

<?php

namespace Drupal\entity_contact_email\Service;

use Drupal\entity_contact\EntityContactMessageInterface;

/**
 * The mailer interface.
 */
interface MailerInterface {

  /**
   * Send all configured e-mails for the given contact form submission.
   *
   * @param \Drupal\entity_contact\EntityContactMessageInterface $message
   *   The contact form submission.
   */
  public function sendEmailsForEntityContactMessage(EntityContactMessageInterface $message): void;

}

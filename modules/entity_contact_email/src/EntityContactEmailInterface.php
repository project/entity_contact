<?php

namespace Drupal\entity_contact_email;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\entity_contact\EntityContactFormInterface;

/**
 * Provides an interface defining a contact form entity.
 */
interface EntityContactEmailInterface extends ConfigEntityInterface {

  /**
   * Get the entity contact form.
   *
   * @return \Drupal\entity_contact\EntityContactFormInterface
   *   The entity contact form
   */
  public function getEntityContactForm(): EntityContactFormInterface;

  /**
   * Set the entity contact form.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entityContactForm
   *   The entity contact form.
   */
  public function setEntityContactForm(EntityContactFormInterface $entityContactForm): void;

  /**
   * Get the e-mail subject.
   *
   * @return string
   *   The subject.
   */
  public function getSubject();

  /**
   * Set the e-mail subject.
   *
   * @param string $subject
   *   The subject.
   */
  public function setSubject(string $subject): void;

  /**
   * Returns the e-mail body.
   *
   * @return mixed
   *   The body
   */
  public function getBody();

  /**
   * Sets the e-mail body.
   *
   * @param string $body
   *   The body.
   *
   * @return $this
   */
  public function setBody($body);

  /**
   * Get the recipients.
   *
   * If a recipient is not a valid e-mail address, it is interpreted as a
   * field name, and the field value from the entity_contact_message entity
   * is used as a recipient instead.
   *
   * @return string[]
   *   The recipients.
   */
  public function getRecipients(): array;

  /**
   * Set the recipients.
   *
   * @param string[] $recipients
   *   The recipients.
   *
   * @see getRecipients()
   */
  public function setRecipients(array $recipients): void;

  /**
   * Get all recipients which are e-mail addresses.
   *
   * @return string[]
   *   The e-mail addresses.
   */
  public function getRecipientEmailAddresses(): array;

  /**
   * Get all recipient which are valid field names.
   *
   * @return string[]
   *   The field names
   */
  public function getRecipientFieldNames(): array;

}

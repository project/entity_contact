<?php

namespace Drupal\entity_contact_email;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Render\Markup;
use Drupal\entity_contact\EntityContactFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of contact form email entities.
 */
class EntityContactEmailListBuilder extends ConfigEntityListBuilder {

  /**
   * The entity contact form to list emails for.
   *
   * @var \Drupal\entity_contact\EntityContactFormInterface|null
   */
  protected $entityContactForm;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\entity_contact\EntityContactFormInterface $entityContactForm
   *   The entity contact form to list emails for.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityContactFormInterface $entityContactForm = NULL) {
    parent::__construct($entity_type, $storage);
    $this->entityContactForm = $entityContactForm;
  }

  /**
   * {@inheritDoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    /**
     * @var \Drupal\Core\Routing\RouteMatchInterface $route_match
     */
    $route_match = $container->get('current_route_match');
    /**
     * @var \Drupal\Core\Entity\EntityStorageInterface $entity_contact_form_storage
     */
    $entity_contact_form_storage = $container->get('entity_type.manager')
      ->getStorage('entity_contact_form');

    if (!empty($entity_contact_form = $route_match->getParameter('entity_contact_form'))) {
      $entity_contact_form = $entity_contact_form_storage->load($entity_contact_form);
    }

    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $entity_contact_form
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery();

    if ($this->entityContactForm !== NULL) {
      $query->condition('entityContactForm', $this->entityContactForm->id());
    }

    $query->accessCheck(TRUE)
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['subject'] = $this->t('Subject');
    $header['recipient'] = $this->t('Recipient');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
     * @var \Drupal\entity_contact_email\EntityContactEmailInterface $entity
     */
    $row['label'] = $entity->label();
    $row['subject'] = $entity->getSubject();

    $recipients = [];
    foreach ($entity->getRecipientEmailAddresses() as $recipient) {
      $recipients[] = $recipient;
    }
    foreach ($entity->getRecipientFieldNames() as $recipient) {
      $recipients[] = entity_contact_email_get_field_recipient_label($entity->getEntityContactForm(), $recipient);
    }
    $row['recipients'] = Markup::create(implode(', ', $recipients));

    return $row + parent::buildRow($entity);
  }

}

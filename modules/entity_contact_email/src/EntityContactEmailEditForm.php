<?php

namespace Drupal\entity_contact_email;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\entity_contact\Entity\EntityContactForm;
use Drupal\entity_contact\Entity\EntityContactMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base form for contact form edit forms.
 *
 * @internal
 */
class EntityContactEmailEditForm extends EntityForm implements ContainerInjectionInterface {

  use ConfigFormBaseTrait;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Constructs a new ContactFormEditForm.
   *
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   */
  public function __construct(EmailValidatorInterface $email_validator) {
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntity() {
    parent::prepareEntity();

    /**
     * @var \Drupal\entity_contact_email\EntityContactEmailInterface $entity
     */
    $entity = $this->entity;

    if ($entity->isNew()) {
      $entity_contact_form = $this->entityTypeManager->getStorage('entity_contact_form')
        ->load($this->getEntityFromRouteMatch($this->getRouteMatch(), 'entity_contact_form'));

      $entity->setEntityContactForm($entity_contact_form);
    }
  }

  /**
   * Title callback.
   *
   * @return string
   *   The title.
   */
  public function title(): string {
    $entity_contact_form = EntityContactForm::load($this->getEntityFromRouteMatch($this->getRouteMatch(), 'entity_contact_form'));

    return $this->t('Add email to @contact_form', ['@contact_form' => $entity_contact_form->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /**
     * @var \Drupal\entity_contact_email\EntityContactEmailInterface $email
     */
    $email = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('This label is for administrative use only'),
      '#maxlength' => 255,
      '#default_value' => $email->label(),
      '#required' => TRUE,
      '#weight' => 1,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $email->isNew() ? uniqid() : $email->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => '\Drupal\entity_contact\Entity\EntityContactForm::load',
      ],
      '#disabled' => !$email->isNew(),
      '#weight' => 2,
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#maxlength' => 255,
      '#default_value' => $email->getSubject(),
      '#required' => TRUE,
      '#weight' => 10,
    ];

    $body = $email->getBody();
    $form['body'] = [
      '#type' => 'text_format',
      '#format' => $body['format'] ?? 'html',
      '#title' => $this->t('Body'),
      '#default_value' => $body['value'] ?? '',
      '#weight' => 11,
    ];

    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        'entity_contact_email',
        'entity_contact_form',
        'entity_contact_message',
      ],
      '#weight' => 20,
    ];

    $recipient_type_options = [
      'email' => $this->t('Will be one or more static e-mail addresses'),
      'field' => $this->t('Will be determined by the value of a form field'),
    ];

    $recipient_type = array_keys($recipient_type_options)[0];
    $recipient_email_addresses = $email->getRecipientEmailAddresses();
    $recipient_email_field = NULL;
    foreach ($email->getRecipients() as $recipient) {
      if ($this->emailValidator->isValid($recipient)) {
        $recipient_type = 'email';
      }
      else {
        $recipient_type = 'field';
        $recipient_email_field = $recipient;
      }
    }

    $recipient_type_field_options = [];
    $message = EntityContactMessage::create([
      'entity_contact_form' => $email->getEntityContactForm()->id(),
    ]);
    foreach ($message->getFields(TRUE) as $field_name => $field) {
      $field_definition = $field->getFieldDefinition();
      if ($field_definition->getType() !== 'email') {
        continue;
      }
      $recipient_type_field_options[$field_name] = $field_definition->getLabel() . ' (' . $field_name . ')';
    }

    if (count($recipient_type_field_options) === 0) {
      $recipient_type = 'email';
      unset($recipient_type_options['field']);
    }
    $form_state_recipient_type = $form_state->getUserInput()['recipient_type'] ?? NULL;

    if (!empty($form_state_recipient_type)) {
      $recipient_type = $form_state_recipient_type;
    }

    $form['recipient_type'] = [
      '#type' => 'select',
      '#title' => $this->t('The recipient of this e-mail'),
      '#options' => $recipient_type_options,
      '#default_value' => $recipient_type,
      '#weight' => 30,
      '#ajax' => [
        'callback' => '::recipientTypeAjaxCallback',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];
    if ($recipient_type === 'email') {
      $form['recipient_type_email'] = [
        '#type' => 'textarea',
        '#title' => $this->t('E-mail address(es)'),
        '#description' => $this->t('Enter one e-mail address per line.'),
        '#default_value' => implode("\n", $recipient_email_addresses),
        '#required' => TRUE,
        '#weight' => 31,
        '#element_validate' => ['::validateRecipientTypeEmail'],
      ];
    }
    if ($recipient_type === 'field') {
      $form['recipient_type_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Field'),
        '#options' => $recipient_type_field_options,
        '#default_value' => !empty($recipient_email_field) ? $recipient_email_field : (count($recipient_type_field_options) == 1 ? array_keys($recipient_type_field_options)[0] : NULL),
        '#required' => TRUE,
        '#weight' => 32,
      ];
    }
    $form['#prefix'] = '<div id="' . Html::cleanCssIdentifier($this->getFormId()) . '">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  /**
   * Validate the recipients.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateRecipientTypeEmail(array $element, FormStateInterface $form_state) {
    $emails = explode("\n", $element['#value']);
    $validated_emails = [];
    foreach ($emails as $email) {
      $email = trim($email);
      if (!$this->emailValidator->isValid($email)) {
        $form_state->setError($element, $this->t('@email is not a valid e-mail address.', ['@email' => $email]));
      }
      $validated_emails[] = $email;
    }

    $form_state->setValueForElement($element, implode("\n", $validated_emails));
  }

  /**
   * Ajax callback for the recipient_type field.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response
   */
  public function recipientTypeAjaxCallback(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#' . Html::cleanCssIdentifier($this->getFormId()), $form));
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    $form = parent::afterBuild($element, $form_state);
    $form['actions']['#weight'] = 1000;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\entity_contact_email\EntityContactEmailInterface $email
     */
    $email = $this->entity;

    switch ($form_state->getValue('recipient_type')) {
      case 'email':
        $email->setRecipients(explode("\n", $form_state->getValue('recipient_type_email')));
        break;

      case 'field':
        $email->setRecipients([
          $form_state->getValue('recipient_type_field'),
        ]);
        break;

      default:
        throw new \InvalidArgumentException($form_state->getValue('recipient_type'));
    }

    $status = $email->save();

    if ($status == SAVED_UPDATED) {
      $this->messenger()
        ->addStatus($this->t('E-mail %label has been updated.', ['%label' => $email->label()]));
      $this->logger('entity_contact_email')
        ->notice('E-mail %label has been updated.', [
          '%label' => $email->label(),
        ]);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('E-mail %label has been added.', ['%label' => $email->label()]));
      $this->logger('entity_contact_email')
        ->notice('E-mail %label has been added.', [
          '%label' => $email->label(),
        ]);
    }

    return $status;
  }

}

<?php

namespace Drupal\entity_contact_example_submission_handler\Plugin\entity_contact\SubmissionHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\entity_contact\EntityContactMessageInterface;
use Drupal\entity_contact\SubmissionHandlerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin checks for a field value condition.
 *
 * @EntityContactSubmissionHandler(
 *   id = "entity_contact_example_submission_handler",
 *   title = @Translation("Send by e-mail"),
 *   description = @Translation("Enter an e-mail address where form submissions
 *   will be sent to.")
 * )
 */
class EmailSubmissionHandler extends SubmissionHandlerPluginBase {

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidator
   */
  protected $emailValidator;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The token utility.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Get the default email subject.
   *
   * @return string
   *   The subject
   */
  protected function getDefaultSubject(): string {
    return $this->t('[entity_contact_form:label] form submitted at [entity_contact_message:submission-url]');
  }

  /**
   * Get the default email message.
   *
   * @return string
   *   The message.
   */
  protected function getDefaultMessage(): string {
    return '[entity_contact_message:list-fields]';
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $container);
    $this->emailValidator = $container->get('email.validator');
    $this->mailManager = $container->get('plugin.manager.mail');
    $this->languageManager = $container->get('language_manager');
    $this->renderer = $container->get('renderer');
    $this->token = $container->get('token');
  }

  /**
   * {@inheritDoc}
   */
  public function handle(EntityContactMessageInterface $message): bool {
    $entity_contact_form = $message->getContactForm();
    $recipient = $entity_contact_form->getThirdPartySetting('entity_contact_example_submission_handler', 'submission_handler_email');
    $current_langcode = $this->languageManager->getCurrentLanguage()->getId();

    if (!$this->emailValidator->isValid($recipient)) {
      $this->logger->error($recipient . ' is not a valid e-mail address for contact form ' . $entity_contact_form->id());
      return FALSE;
    }

    $email_subject = $entity_contact_form->getThirdPartySetting('entity_contact_example_submission_handler', 'submission_handler_email_subject', $this->getDefaultSubject());
    $email_subject = $this->token->replace($email_subject, [
      'entity_contact_message' => $message,
      'entity_contact_form' => $entity_contact_form,
    ]);

    $submission_handler_email_message = $entity_contact_form->getThirdPartySetting('entity_contact_example_submission_handler', 'submission_handler_email_message');
    $email_message = [
      '#type' => 'processed_text',
      '#text' => $submission_handler_email_message['value'] ?? $this->getDefaultMessage(),
      '#format' => $submission_handler_email_message['format'] ?? NULL,
    ];

    $email_message['#text'] = $this->token->replace($email_message['#text'], [
      'entity_contact_message' => $message,
      'entity_contact_form' => $entity_contact_form,
    ]);

    $params = [
      'contact_form_message' => $message,
      'contact_form' => $entity_contact_form,
      'recipient' => $recipient,
      'subject' => $email_subject,
      'body' => $this->renderer->renderplain($email_message),
    ];

    $this->mailManager->mail('entity_contact', 'submission_handler_email', $recipient, $current_langcode, $params);

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsForm(EntityContactFormInterface $entity_contact_form, FormStateInterface $form_state): array {
    $submission_handler_email_message = $entity_contact_form->getThirdPartySetting('entity_contact_example_submission_handler', 'submission_handler_email_message');
    return [
      'email' => [
        '#type' => 'email',
        '#title' => $this->t('E-mail address of recipient'),
        '#default_value' => $entity_contact_form->getThirdPartySetting('entity_contact_example_submission_handler', 'submission_handler_email'),
      ],
      'submission_handler_email' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Body'),
        'submission_handler_email_subject' => [
          '#type' => 'textfield',
          '#title' => $this->t('Subject'),
          '#default_value' => $entity_contact_form->getThirdPartySetting('entity_contact_example_submission_handler', 'submission_handler_subject', $this->getDefaultSubject()),
        ],
        'submission_handler_email_message' => [
          '#type' => 'text_format',
          '#format' => $submission_handler_email_message['format'] ?? 'html',
          '#title' => $this->t('Message'),
          '#default_value' => $submission_handler_email_message['value'] ?? $this->getDefaultMessage(),
        ],
        'submission_handler_email_message_token_help' => [
          '#theme' => 'token_tree_link',
          '#token_types' => [
            'entity_contact_form',
            'entity_contact_message',
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function validateSettingsForm(EntityContactFormInterface $entity_contact_form, array $form, FormStateInterface $form_state): void {
    $email = $form_state->getValue('email');
    if (!$this->emailValidator->isValid($email)) {
      $form_state->setErrorByName('email', $this->t('The e-mail address is not valid.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitSettingsForm(EntityContactFormInterface $entity_contact_form, FormStateInterface $form_state): void {
    $email = $form_state->getValue('email');
    $entity_contact_form->setThirdPartySetting('entity_contact_example_submission_handler', 'submission_handler_email', $email);
    $entity_contact_form->setThirdPartySetting('entity_contact_example_submission_handler', 'submission_handler_email_message', $form_state->getValue('submission_handler_email_message'));
    $entity_contact_form->save();
  }

}

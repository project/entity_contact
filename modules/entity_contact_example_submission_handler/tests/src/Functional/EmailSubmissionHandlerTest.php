<?php

namespace Drupal\Tests\entity_contact_example_submission_handler\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\entity_contact\Functional\EntityContactTestBase;

/**
 * Test the email submission handler plugin.
 *
 * @group entity_contact_example_submission_handler
 */
class EmailSubmissionHandlerTest extends EntityContactTestBase {

  use AssertMailTrait;
  use StringTranslationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_contact_example_submission_handler',
  ];

  /**
   * Test the email submission handler.
   */
  public function testEmailSubmissionHandler(): void {
    $this->drupalLogin($this->adminUser);

    // Create a simple textfield in the feedback form.
    $field_name = mb_strtolower($this->randomMachineName());
    $field_label = $this->randomMachineName();
    $this->fieldUIAddNewField('admin/content/entity-contact/manage/feedback', $field_name, $field_label, 'string');

    drupal_flush_all_caches();

    $field_name = 'field_' . $field_name;
    $field_name_value = $this->randomMachineName();

    // Email submission handler is disabled by default.
    $this->drupalGet('contact-form/feedback');
    $this->submitForm([
      $field_name . '[0][value]' => $field_name_value,
    ], 'Send');

    $this->assertCount(0, $this->getMails());

    /**
     * @var \Drupal\Core\State\StateInterface $state
     */
    $state = $this->container->get('state');
    $state->resetCache();

    // Enable the email submission handler.
    $this->drupalGet('admin/content/entity-contact/manage/feedback/handlers/entity_contact_example_submission_handler');
    $this->submitForm([
      'enabled' => TRUE,
    ], 'Save');

    $this->assertSession()
      ->pageTextContains('The e-mail address is not valid.');

    $this->drupalGet('admin/content/entity-contact/manage/feedback/handlers/entity_contact_example_submission_handler');
    $this->submitForm([
      'enabled' => TRUE,
      'email' => 'simpletest@example.com',
    ], 'Save');

    $this->drupalGet('contact-form/feedback');
    $this->submitForm([
      $field_name . '[0][value]' => $field_name_value,
    ], 'Send');

    $this->assertMailString('body', $field_name_value, 1);
  }

}

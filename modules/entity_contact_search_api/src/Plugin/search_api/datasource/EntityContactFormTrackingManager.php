<?php

namespace Drupal\entity_contact_search_api\Plugin\search_api\datasource;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\search_api\Task\TaskManagerInterface;

/**
 * Tracks changes in entity_contact_form entities.
 */
class EntityContactFormTrackingManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Search API task manager.
   *
   * @var \Drupal\search_api\Task\TaskManagerInterface
   */
  protected $taskManager;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\search_api\Task\TaskManagerInterface $taskManager
   *   The task manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, TaskManagerInterface $taskManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
    $this->taskManager = $taskManager;
  }

  /**
   * Implements hook_entity_insert().
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity
   *   The new entity.
   */
  public function entityInsert(EntityContactFormInterface $entity) {
    $indexes = $this->getIndexesForEntity($entity);

    foreach ($indexes as $index) {
      $index->trackItemsInserted($entity->getEntityTypeId(), [$entity->id()]);
    }
  }

  /**
   * Implements hook_entity_update().
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity
   *   The updated entity.
   */
  public function entityUpdate(EntityContactFormInterface $entity) {
    $indexes = $this->getIndexesForEntity($entity);

    foreach ($indexes as $index) {
      $index->trackItemsUpdated($entity->getEntityTypeId(), [$entity->id()]);
    }
  }

  /**
   * Implements hook_entity_delete().
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity
   *   The deleted entity.
   */
  public function entityDelete(EntityContactFormInterface $entity) {
    $indexes = $this->getIndexesForEntity($entity);

    foreach ($indexes as $index) {
      $index->trackItemsDeleted($entity->getEntityTypeId(), [$entity->id()]);
    }
  }

  /**
   * Retrieves all indexes that are configured to index the given entity.
   *
   * @param \Drupal\entity_contact\EntityContactFormInterface $entity
   *   The entity for which to check.
   *
   * @return \Drupal\search_api\IndexInterface[]
   *   All indexes that are configured to index the given entity.
   */
  public function getIndexesForEntity(EntityContactFormInterface $entity): array {
    $datasource_id = $entity->getEntityTypeId();

    /** @var \Drupal\search_api\IndexInterface[] $indexes */
    $indexes = [];
    try {
      $indexes = $this->entityTypeManager->getStorage('search_api_index')
        ->loadMultiple();
    }
    catch (InvalidPluginDefinitionException $e) {
      // Can't really happen, but play it safe to appease static code analysis.
    }
    catch (PluginNotFoundException $e) {
      // Can't really happen, but play it safe to appease static code analysis.
    }

    foreach ($indexes as $index_id => $index) {
      // Filter out indexes that don't contain the datasource in question.
      if (!$index->isValidDatasource($datasource_id)) {
        unset($indexes[$index_id]);
      }
    }

    return $indexes;
  }

}

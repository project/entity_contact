<?php

namespace Drupal\entity_contact_search_api\Plugin\search_api\datasource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\entity_contact\EntityContactFormInterface;
use Drupal\search_api\Datasource\DatasourcePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Represents a datasource which exposes the entity_contact_form entities.
 *
 * @SearchApiDatasource(
 *   id = "entity_contact_form",
 *   entity_type = "entity_contact_form",
 *   label = "entity_contact_form",
 *   description = "Provides Entity Contact Form entities."
 * )
 */
class EntityContactForm extends DatasourcePluginBase implements ContainerFactoryPluginInterface {

  /**
   * Entity_contact_form storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->storage = $entityTypeManager->getStorage('entity_contact_form');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getPropertyDefinitions() {
    return [
      'label' => new DataDefinition([
        'type' => 'string',
        'label' => $this->t('Label'),
        'description' => '',
        'read-only' => TRUE,
        'computed' => FALSE,
        'required' => TRUE,
      ]),
      'description' => new DataDefinition([
        'type' => 'search_api_html',
        'label' => $this->t('Description'),
        'description' => '',
        'read-only' => TRUE,
        'computed' => FALSE,
        'required' => TRUE,
      ]),
      'message' => new DataDefinition([
        'type' => 'string',
        'label' => $this->t('Message'),
        'description' => '',
        'read-only' => TRUE,
        'computed' => FALSE,
        'required' => TRUE,
      ]),
      'submit_button_text' => new DataDefinition([
        'type' => 'string',
        'label' => $this->t('Submit button text'),
        'description' => '',
        'read-only' => TRUE,
        'computed' => FALSE,
        'required' => TRUE,
      ]),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function load($id) {
    $entity_contact_form = $this->storage->load($id);

    if ($entity_contact_form instanceof EntityContactFormInterface) {
      return $entity_contact_form->getTypedData();
    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function loadMultiple(array $ids) {
    $entity_contact_forms = $this->storage->loadMultiple($ids);

    $items = [];
    foreach ($entity_contact_forms as $id => $entity_contact_form) {
      $items[$id] = $entity_contact_form->getTypedData();
    }

    return $items;
  }

  /**
   * {@inheritDoc}
   */
  public function getItemLanguage(ComplexDataInterface $item) {
    $value = $item->getValue();
    return $value instanceof EntityContactFormInterface ? $value->language()
      ->getId() : LanguageInterface::LANGCODE_NOT_SPECIFIED;
  }

  /**
   * {@inheritDoc}
   */
  public function getItemId(ComplexDataInterface $item) {
    $value = $item->getValue();
    return $value instanceof EntityContactFormInterface ? $value->id() : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getItemLabel(ComplexDataInterface $item) {
    $value = $item->getValue();
    return $value instanceof EntityContactFormInterface ? $value->label() : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityTypeId() {
    return 'entity_contact_form';
  }

  /**
   * {@inheritDoc}
   */
  public function getItemIds($page = NULL) {
    if ($page > 0) {
      return NULL;
    }

    return $this->storage->getQuery()
      ->accessCheck(FALSE)
      ->execute();
  }

}

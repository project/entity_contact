Entity Contact is a bare version of the core Contact module, which doesn't
force name and e-mail fields and allows optional storage of submissions
by default.

It also contains submodules to supply additional functionalities:

entity_contact_email: Send e-mails after a form has been submitted
entity_contact_example_submission_handler: Example form submission handler.
entity_contact_export: Allows users to create exports of form submissions.
entity_contact_route: Expose forms on an url.
entity_contact_search_api: Make forms searchable using the search_api module.
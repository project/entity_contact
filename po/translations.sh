#!/bin/sh

drush potion:generate nl /var/www/ciz/web/modules/custom/entity_contact /var/www/ciz/web/modules/custom/entity_contact/po/ --recursive
drush potion:fill nl /var/www/ciz/web/modules/custom/entity_contact/po/nl.po --overwrite
